"""add hud time scope settings

Revision ID: 272d9e883887
Revises: a50e0db89eb9
Create Date: 2020-05-25 19:52:50.166862

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "272d9e883887"
down_revision = "a50e0db89eb9"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "hero_time_scope",
                sa.Enum("ALL_TIME", "CURRENT_SESSION", name="hudtimescope"),
            )
        )
        batch_op.add_column(
            sa.Column(
                "opponent_time_scope",
                sa.Enum("ALL_TIME", "CURRENT_SESSION", name="hudtimescope"),
            )
        )
    op.execute("UPDATE applicationsettings SET hero_time_scope = 'ALL_TIME'")
    op.execute("UPDATE applicationsettings SET opponent_time_scope = 'ALL_TIME'")

    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.alter_column(
            "hero_time_scope",
            existing_type=sa.Enum("ALL_TIME", "CURRENT_SESSION", name="hudtimescope"),
            nullable=False,
        )
        batch_op.alter_column(
            "opponent_time_scope",
            existing_type=sa.Enum("ALL_TIME", "CURRENT_SESSION", name="hudtimescope"),
            nullable=False,
        )


def downgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.drop_column("opponent_time_scope")
        batch_op.drop_column("hero_time_scope")
