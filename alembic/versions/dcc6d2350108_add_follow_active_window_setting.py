"""add follow_active_window setting

Revision ID: dcc6d2350108
Revises: 5f3def1f50cc
Create Date: 2020-05-09 13:15:44.077026

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "dcc6d2350108"
down_revision = "5f3def1f50cc"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.add_column(sa.Column("follow_active_window", sa.Boolean()))
    op.execute("UPDATE applicationsettings SET follow_active_window = TRUE")
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.alter_column(
            "follow_active_window", existing_type=sa.Boolean(), nullable=False,
        )


def downgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.drop_column("follow_active_window")
