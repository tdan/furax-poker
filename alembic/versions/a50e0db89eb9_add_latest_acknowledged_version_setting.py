"""add latest_acknowledged_version setting

Revision ID: a50e0db89eb9
Revises: dcc6d2350108
Create Date: 2020-05-17 11:15:41.496434

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a50e0db89eb9"
down_revision = "dcc6d2350108"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("latest_acknowledged_version", sa.String(), nullable=True)
        )


def downgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.drop_column("latest_acknowledged_version")
