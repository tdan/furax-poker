"""add hide_hud_when_window_not_active setting

Revision ID: 232874e3661c
Revises: a8969a1e7ed1
Create Date: 2020-05-08 11:19:12.903903

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "232874e3661c"
down_revision = "a8969a1e7ed1"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.add_column(sa.Column("hide_hud_when_window_not_active", sa.Boolean()))
    op.execute("UPDATE applicationsettings SET hide_hud_when_window_not_active = TRUE")
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.alter_column(
            "hide_hud_when_window_not_active",
            existing_type=sa.Boolean(),
            nullable=False,
        )


def downgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.drop_column("hide_hud_when_window_not_active")
