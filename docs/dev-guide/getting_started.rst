.. _dev-guide-getting-started:

===============
Getting started
===============

Furax Poker is a Python application using Qt as GUI library and SQLite as
database.

Installation
============

Requirements
------------

- Git
- Python 3.8.2

Instructions
------------

Get source code from repository_ using git and install it from source using
pip with development dependencies.

.. _repository: https://gitlab.com/etiennecrb/furax-poker

.. code-block:: text

  pip install -e ".[dev]"

Usage
=====

Start Furax Poker application using the ``furax`` command:

.. code-block:: text

  furax

Test suite is run using pytest_, tests are located in the ``tests`` folder.

.. _pytest: https://docs.pytest.org

.. code-block:: text

   pytest tests

To contribute, please set up pre-commit hook to automatically format code using
black_:

.. _black: https://black.readthedocs.io/en/stable/

.. code-block:: text

  pre-commit install
