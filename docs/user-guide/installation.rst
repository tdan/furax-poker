.. _user-guide-installation:

============
Installation
============

Furax Poker is available on Windows and Linux (Debian and derivatives and
Fedora). The software could probably be successfully built and run from source
on other platforms (see :ref:`dev-guide` for more information).

Download
========

Download the latest version for your platform on the `releases page`_.

Installing on Windows
=====================

On Windows, Furax Poker is just a plain executable file. Uncompress the
downloaded archive somewhere on your computer and double click on the
``furax_poker.exe`` file. During first launch, you might get a warning
preventing Furax Poker to run because it is not a recognized application. Click
on "More info" and "Run anyway" to start the app.

.. image:: windows_warning.png

Installing on Linux
===================

On Linux, Furax Poker is just a plain executable file. Uncompress the
downloaded archive somewhere on your compute and double click on the
``furax_poker`` file.

Updating
========

Furax Poker doesn't have an automatic update system. Check regularly the
`releases page`_ if a new version is available!

When updating, you can just remove the previous version, your data is stored
elsewhere on your computer and won't be lost.

.. _releases page: https://gitlab.com/etiennecrb/furax-poker/-/releases
