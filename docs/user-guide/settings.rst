.. _user-guide-settings:

========
Settings
========

Open the settings dialog from the File menu or hit ``Ctrl+Alt+S``.

History folder
==============

.. image:: settings_dialog_folder.png

Furax Poker works by analyzing the files where your hand history is stored. You
must configure the folder that the app will scan, looking for hands to make
the HUD works. For Winamax, this folder is usually
``Documents/Winamax Poker/accounts/<Your account>/history``.

**History folder**
  Folder where your hand history files are stored.

**Room**
  Poker room of hand history so that Furax Poker knows how to parse these files
  (currently only Winamax is supported).

**Hero nickname**
  Not used for now.

Behavior
========

.. image:: settings_dialog_behavior.png

**Automatically start HUD on new table**
  Enable this option to automatically display HUD for every table you join.

**Favorite seat**
  Indicate Furax Poker that your seat is always at the same place on the
  screen. It is strongly advise to configure a favorite seat in your poker
  software so that you don't have to replace all HUD windows each time your
  table changes during a tournament.

**Hide HUD when window is not active**
  Enable this option to hide HUD windows when your poker software is not the
  active window on your computer. Note that detection of the active window is
  experimental, don't hesitate to turn off this feature if HUD windows are not
  visible on the screen.

**Follow window position**
  Enable this option to make HUD windows follow your poker software window as
  you move or resize it. Note that this feature is experimental, don't hesitate
  to it turn off if HUD windows positioning is incorrect.

Statistics
==========

.. image:: settings_dialog_statistics.png

**Hero HUD**
  This option allows you to decide if you want to display your all-time
  statistics, or only the statistics for your current session (current
  tournament or cash game table).

**Opponent HUD**
  This option is similar to Hero HUD and allows you to set the time scope of
  opponent statistics.
