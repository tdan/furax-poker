===========
Furax Poker
===========

Furax Poker is a poker HUD (head-up display) software. It monitors your hand
history folder in real time to display statistics about your opponents. Furax
Poker works on Linux and Windows, the only supported poker room is Winamax but
others could easily be added.

For more information about how to use Furax Poker and how to contribute, please
visit https://furax-poker.readthedocs.io.

License
=======
GPL_

.. _GPL: ./LICENSE.txt
