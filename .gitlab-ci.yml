image: python:3.8.2-buster

.pip-install:
  before_script:
    - apt -y update && apt -y upgrade
    - apt install --no-install-recommends -y xauth xvfb
    - pip install -e ".[dev]"

stages:
  - test
  - build

conventional-commits:
  stage: test
  image: commitsar/commitsar:latest
  script:
    - commitsar

linting:
  stage: test
  before_script:
    - pip install flake8
  script:
    - flake8 .

mypy:
  extends: .pip-install
  stage: test
  script:
    - mypy furax_poker

black:
  stage: test
  before_script:
    - pip install black
  script:
    - black . --check

alembic:
  stage: test
  before_script:
    - pip install alembic
  script:
    - HEAD_AMOUNT=$(alembic heads | wc -l)
    - if [ $HEAD_AMOUNT -ne 1 ]; then echo "Divergence detected in alembic migration."; exit 1; fi

test:
  extends: .pip-install
  stage: test
  variables:
    QT_QPA_PLATFORM: "offscreen"
  script:
    - xvfb-run -a pytest

.build:
  stage: build
  artifacts:
    name: furax-poker-$CI_COMMIT_REF_NAME-$CI_JOB_ID-$PLATFORM
    paths:
      - furax-poker
    expire_in: 1 week

.build-debian:
  extends:
    - .pip-install
    - .build
  variables:
    PLATFORM: debian
  script:
    - pyinstaller --hidden-import='pkg_resources.py2_warn' --hidden-import='sqlalchemy.ext.baked' --add-data 'data:data' --add-data 'logging.conf:.' --add-data 'furax_poker.ini:.' --add-data 'alembic:alembic' --onefile --name furax_poker furax_poker/gui/main.py
  after_script:
    - mkdir furax-poker
    - mv dist/* furax-poker/

build-debian:
  extends: .build-debian
  only:
    - tags

build-debian-manual:
  extends: .build-debian
  when: manual
  except:
    - tags

.build-fedora:
  extends: .build
  image: fedora:latest
  variables:
    PLATFORM: fedora
  before_script:
    - dnf -y update && dnf -y upgrade
    - dnf install -y python38 wget binutils
    - wget https://bootstrap.pypa.io/get-pip.py
    - python3.8 get-pip.py
    - python3.8 -m venv .venv
    - source .venv/bin/activate
    - pip install --upgrade pip
    - pip install -e ".[dev]"
  script:
    - pyinstaller --hidden-import='pkg_resources.py2_warn' --hidden-import='sqlalchemy.ext.baked' --add-data 'data:data' --add-data 'logging.conf:.' --add-data 'furax_poker.ini:.' --add-data 'alembic:alembic' --onefile --name furax_poker furax_poker/gui/main.py
  after_script:
    - mkdir furax-poker
    - mv dist/* furax-poker/

build-fedora:
  extends: .build-fedora
  only:
    - tags

build-fedora-manual:
  extends: .build-fedora
  when: manual
  except:
    - tags

.build-windows:
  extends: .build
  tags:
    - windows
  variables:
    PLATFORM: windows
  before_script:
    - choco install python --version 3.8.2 -y
    - $env:PATH="C:\Python38;C:\Python38\Scripts;$env:PATH"
    - Invoke-WebRequest -Uri 'https://bootstrap.pypa.io/get-pip.py' -OutFile 'get-pip.py'
    - python get-pip.py --disable-pip-version-check --no-cache-dir
    - pip install -e ".[dev]"
  script:
    - pyinstaller --windowed --hidden-import='pkg_resources.py2_warn' --hidden-import='sqlalchemy.ext.baked' --add-data 'data;data' --add-data 'logging.conf;.' --add-data 'furax_poker.ini;.' --add-data 'alembic;alembic' --onefile --name furax_poker furax_poker/gui/main.py
  after_script:
    - mkdir furax-poker
    - Move-Item -Path .\dist\* -Destination .\furax-poker\

build-windows:
  extends: .build-windows
  only:
    - tags

build-windows-manual:
  extends: .build-windows
  when: manual
  except:
    - tags
