import pytest

from furax_poker.analyzer import analyze_hand
from furax_poker.fact import FactMetrics
from furax_poker.hand import (
    ActionType,
    Player,
    Seat,
    SeatPosition,
    Table,
    Tournament,
)
from furax_poker.settings import TimeScope


def test_default_values():
    metrics = FactMetrics()
    assert metrics.hand_played == 0
    assert metrics.preflop_check == 0
    assert metrics.preflop_call == 0
    assert metrics.preflop_bet == 0
    assert metrics.preflop_raise == 0
    assert metrics.preflop_fold == 0
    assert metrics.flop_check == 0
    assert metrics.flop_call == 0
    assert metrics.flop_bet == 0
    assert metrics.flop_raise == 0
    assert metrics.flop_fold == 0
    assert metrics.turn_check == 0
    assert metrics.turn_call == 0
    assert metrics.turn_bet == 0
    assert metrics.turn_raise == 0
    assert metrics.turn_fold == 0
    assert metrics.river_check == 0
    assert metrics.river_call == 0
    assert metrics.river_bet == 0
    assert metrics.river_raise == 0
    assert metrics.river_fold == 0
    assert metrics.vpip_chance == 0
    assert metrics.vpip == 0
    assert metrics.pfr == 0
    assert metrics.cbet_chance == 0
    assert metrics.cbet == 0
    assert metrics.fold_to_cbet_chance == 0
    assert metrics.fold_to_cbet == 0


def test_vpip_obs():
    metrics = FactMetrics(vpip_chance=0, vpip=0)
    assert metrics.vpip_obs == 0

    metrics = FactMetrics(vpip_chance=3, vpip=5)
    assert metrics.vpip_obs == 8


def test_vpip_rate():
    metrics = FactMetrics(vpip_chance=0, vpip=0)
    assert metrics.vpip_rate is None

    metrics = FactMetrics(vpip_chance=3, vpip=5)
    assert metrics.vpip_rate == 5 / 8


def test_pfr_rate():
    metrics = FactMetrics(vpip_chance=0, pfr=0)
    assert metrics.pfr_rate is None

    metrics = FactMetrics(vpip_chance=10, pfr=5)
    assert metrics.pfr_rate == 5 / 10

    metrics = FactMetrics(vpip_chance=10, pfr=5, vpip=3)
    assert metrics.pfr_rate == 5 / 13


def test_af():
    metrics = FactMetrics()
    assert metrics.af is None

    metrics = FactMetrics(preflop_bet=1, preflop_raise=0, flop_bet=2)
    assert metrics.af is None

    metrics = FactMetrics(preflop_call=4, flop_call=2, turn_call=1, river_call=0)
    assert metrics.af == 0

    metrics = FactMetrics(
        preflop_bet=1,
        preflop_raise=1,
        flop_bet=2,
        preflop_call=4,
        flop_call=2,
        turn_call=1,
        river_call=0,
    )
    assert metrics.af == 4 / 7


def test_cbet_obs():
    metrics = FactMetrics(cbet_chance=0, cbet=0)
    assert metrics.cbet_obs == 0

    metrics = FactMetrics(cbet_chance=3, cbet=5)
    assert metrics.cbet_obs == 8


def test_cbet_rate():
    metrics = FactMetrics(cbet_chance=0, cbet=0)
    assert metrics.cbet_rate is None

    metrics = FactMetrics(cbet_chance=3, cbet=5)
    assert metrics.cbet_rate == 5 / 8


def test_fold_to_cbet_obs():
    metrics = FactMetrics(fold_to_cbet_chance=0, fold_to_cbet=0)
    assert metrics.fold_to_cbet_obs == 0

    metrics = FactMetrics(fold_to_cbet_chance=3, fold_to_cbet=5)
    assert metrics.fold_to_cbet_obs == 8


def test_fold_to_cbet_rate():
    metrics = FactMetrics(fold_to_cbet_chance=0, fold_to_cbet=0)
    assert metrics.fold_to_cbet_rate is None

    metrics = FactMetrics(fold_to_cbet_chance=3, fold_to_cbet=5)
    assert metrics.fold_to_cbet_rate == 5 / 8


@pytest.mark.parametrize(
    "time_scope,is_playing_tournament,expected",
    [
        (
            TimeScope.ALL_TIME,
            False,
            FactMetrics(
                hand_played=3,
                preflop_check=0,
                preflop_call=1,
                preflop_raise=1,
                preflop_fold=1,
                flop_call=1,
                flop_bet=1,
                turn_check=1,
                river_bet=1,
                river_fold=1,
                vpip_chance=1,
                vpip=2,
                pfr=1,
                cbet=1,
            ),
        ),
        (
            TimeScope.ALL_TIME,
            True,
            FactMetrics(
                hand_played=4,
                preflop_check=0,
                preflop_call=2,
                preflop_raise=1,
                preflop_fold=1,
                flop_call=1,
                flop_bet=1,
                turn_check=1,
                river_bet=1,
                river_fold=1,
                vpip_chance=1,
                vpip=3,
                pfr=1,
                cbet=1,
            ),
        ),
        (
            TimeScope.CURRENT_SESSION,
            False,
            FactMetrics(
                hand_played=1,
                preflop_check=0,
                preflop_call=1,
                preflop_raise=0,
                preflop_fold=0,
                flop_call=1,
                flop_bet=0,
                turn_check=0,
                river_bet=0,
                river_fold=0,
                vpip_chance=0,
                vpip=1,
                pfr=0,
                cbet=0,
            ),
        ),
        (
            TimeScope.CURRENT_SESSION,
            True,
            FactMetrics(
                hand_played=2,
                preflop_check=0,
                preflop_call=2,
                preflop_raise=0,
                preflop_fold=0,
                flop_call=1,
                flop_bet=0,
                turn_check=0,
                river_bet=0,
                river_fold=0,
                vpip_chance=0,
                vpip=2,
                pfr=0,
                cbet=0,
            ),
        ),
    ],
)
def test_from_seat(
    session, room, hand_builder, is_playing_tournament, time_scope, expected
):
    hand_builder.seating_plan = [
        (1, "Player 1"),
        (2, "Player 2"),
        (3, "Player 3"),
        (4, "Player 4"),
        (5, "Hero", SeatPosition.BUTTON),
    ]
    hand_1 = hand_builder.create(
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        flop=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.CALL),
        ],
        turn=[("Player 1", ActionType.CHECK), ("Hero", ActionType.CHECK)],
        river=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.RAISE),
            ("Hero", ActionType.FOLD),
        ],
    )
    hand_2 = hand_builder.create(
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.FOLD),
            ("Player 1", ActionType.FOLD),
            ("Player 2", ActionType.COLLECT),
        ],
    )
    if is_playing_tournament:
        hand_builder.tournament = Tournament(room=room, name="Tournament")
        first_tournament_table = Table(room=room, name="First tournament table")
        hand = hand_builder.create(
            table=first_tournament_table, preflop=[("Hero", ActionType.CALL)]
        )
        analyze_hand(session, hand)

    current_table = Table(room=room, name="Current table")
    hand_3 = hand_builder.create(
        table=current_table,
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.CALL),
            ("Player 1", ActionType.FOLD),
            ("Player 2", ActionType.CHECK),
        ],
        flop=[("Player 1", ActionType.BET), ("Hero", ActionType.CALL)],
    )
    for hand in [hand_1, hand_2, hand_3]:
        analyze_hand(session, hand)

    seat = (
        session.query(Seat)
        .join(Player)
        .filter(Player.name == "Hero", Seat.hand_id == hand_3.id)
        .first()
    )
    metrics = FactMetrics.from_seat(session, seat, time_scope=time_scope)

    assert metrics == expected
