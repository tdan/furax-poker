Game Hand #1156446940 - Tournament #26537363 - Holdem(No Limit) - Level 5 (1000.00/2000.00)- 2022/03/15 01:02:39 UTC
Table '6' 9-max Seat #7 is the button
Seat 1: toemittens (29600.00)
Seat 2: CottonPark (29800.00)
Seat 3: SgtBaker20 (50200.00)
Seat 4: r23y (18924.00)
Seat 5: xelnaga90 (43350.00)
Seat 6: TheVioletMystery (19850.00)
Seat 8: Woods1 (90952.00)
Seat 9: poroto1 (29200.00)
toemittens posts ante 200.00
CottonPark posts ante 200.00
SgtBaker20 posts ante 200.00
r23y posts ante 200.00
xelnaga90 posts ante 200.00
TheVioletMystery posts ante 200.00
Woods1 posts ante 200.00
poroto1 posts ante 200.00
Woods1 posts the small blind 1000.00
poroto1 posts the big blind 2000.00
*** HOLE CARDS ***
Main pot 1600.00
Dealt to xelnaga90 [5h Ks]
toemittens folds
CottonPark folds
SgtBaker20 folds
r23y folds
xelnaga90 folds
TheVioletMystery folds
Woods1 calls 1000.00
poroto1 checks
*** FLOP *** [4c 8c Kc]
Main pot 5600.00
Woods1 checks
poroto1 bets 2000.00
Woods1 folds
Uncalled bet (2000.00) returned to poroto1
poroto1 does not show
*** SUMMARY ***
Total pot 5600.00
Board [4c 8c Kc]
Seat 1: toemittens folded on the Pre-Flop and did not bet
Seat 2: CottonPark folded on the Pre-Flop and did not bet
Seat 3: SgtBaker20 folded on the Pre-Flop and did not bet
Seat 4: r23y folded on the Pre-Flop and did not bet
Seat 5: xelnaga90 folded on the Pre-Flop and did not bet
Seat 6: TheVioletMystery folded on the Pre-Flop and did not bet
Seat 8: Woods1 (small blind) folded on the Flop
Seat 9: poroto1 did not show and won 5600.00

Game Hand #1156448134 - Tournament #26537363 - Holdem(No Limit) - Level 6 (1250.00/2500.00)- 2022/03/15 01:03:30 UTC
Table '7' 9-max Seat #4 is the button
Seat 1: r23y (18724.00)
Seat 2: xelnaga90 (43150.00)
Seat 3: Twizy (35300.00)
Seat 4: wsandman28 (29237.00)
Seat 5: gdref (32450.00)
Seat 6: AAngelov180 (86847.00)
Seat 7: Hashmd (25449.00) is sitting out
Seat 8: ViperCity253 (86505.00)
r23y posts ante 250.00
xelnaga90 posts ante 250.00
Twizy posts ante 250.00
wsandman28 posts ante 250.00
gdref posts ante 250.00
AAngelov180 posts ante 250.00
Hashmd posts ante 250.00
ViperCity253 posts ante 250.00
gdref posts the small blind 1250.00
AAngelov180 posts the big blind 2500.00
*** HOLE CARDS ***
Main pot 2000.00
Dealt to xelnaga90 [5d Jd]
Hashmd folds
ViperCity253 raises 5500.00 to 5500.00
r23y raises 18474.00 to 18474.00 and is all-in
xelnaga90 folds
Twizy folds
wsandman28 folds
gdref folds
AAngelov180 folds
ViperCity253 calls 12974.00
*** FLOP *** [7c 4s Td]
Main pot 42698.00
*** TURN *** [7c 4s Td] [2c]
Main pot 42698.00
*** RIVER *** [7c 4s Td 2c] [5h]
Main pot 42698.00
*** SHOW DOWN ***
Main pot 42698.00
r23y shows [Qc Js] (a high card, Queen high [Qc Js Td 7c 5h])
ViperCity253 shows [Ad 9h] (a high card, Ace high [Ad Td 9h 7c 5h])
ViperCity253 collected 42698.00 from main pot
*** SUMMARY ***
Total pot 42698.00
Board [7c 4s Td 2c 5h]
Seat 1: r23y showed [Qc Js] and lost with a high card, Queen high [Qc Js Td 7c 5h]
Seat 2: xelnaga90 folded on the Pre-Flop and did not bet
Seat 3: Twizy folded on the Pre-Flop and did not bet
Seat 4: wsandman28 (button) folded on the Pre-Flop
Seat 5: gdref (small blind) folded on the Pre-Flop
Seat 6: AAngelov180 (big blind) folded on the Pre-Flop
Seat 7: Hashmd folded on the Pre-Flop and did not bet
Seat 8: ViperCity253 showed [Ad 9h] and won 42698.00 with a high card, Ace high [Ad Td 9h 7c 5h]

Game Hand #1156449140 - Tournament #26537363 - Holdem(No Limit) - Level 6 (1250.00/2500.00)- 2022/03/15 01:04:10 UTC
Table '7' 9-max Seat #5 is the button
Seat 2: xelnaga90 (42900.00)
Seat 3: Twizy (35050.00)
Seat 4: wsandman28 (28987.00)
Seat 5: gdref (30950.00)
Seat 6: AAngelov180 (84097.00)
Seat 7: Hashmd (25199.00) is sitting out
Seat 8: ViperCity253 (110479.00)
xelnaga90 posts ante 250.00
Twizy posts ante 250.00
wsandman28 posts ante 250.00
gdref posts ante 250.00
AAngelov180 posts ante 250.00
Hashmd posts ante 250.00
ViperCity253 posts ante 250.00
AAngelov180 posts the small blind 1250.00
Hashmd posts the big blind 2500.00
*** HOLE CARDS ***
Main pot 1750.00
Dealt to xelnaga90 [9h 2d]
ViperCity253 raises 5500.00 to 5500.00
xelnaga90 folds
Twizy folds
wsandman28 folds
gdref folds
AAngelov180 folds
Hashmd folds
Uncalled bet (3000.00) returned to ViperCity253
ViperCity253 does not show
*** SUMMARY ***
Total pot 8000.00
Seat 2: xelnaga90 folded on the Pre-Flop and did not bet
Seat 3: Twizy folded on the Pre-Flop and did not bet
Seat 4: wsandman28 folded on the Pre-Flop and did not bet
Seat 5: gdref (button) folded on the Pre-Flop
Seat 6: AAngelov180 (small blind) folded on the Pre-Flop
Seat 7: Hashmd (big blind) folded on the Pre-Flop
Seat 8: ViperCity253 did not show and won 8000.00

Game Hand #1156450021 - Tournament #26537363 - Holdem(No Limit) - Level 6 (1250.00/2500.00)- 2022/03/15 01:04:46 UTC
Table '7' 9-max Seat #6 is the button
Seat 1: r23y (30000.00)
Seat 2: xelnaga90 (42650.00)
Seat 3: Twizy (34800.00)
Seat 4: wsandman28 (28737.00)
Seat 5: gdref (30700.00)
Seat 6: AAngelov180 (82597.00)
Seat 7: Hashmd (22449.00) is sitting out
Seat 8: ViperCity253 (115729.00)
Seat 9: Out Of Range (30000.00)
r23y posts ante 250.00
xelnaga90 posts ante 250.00
Twizy posts ante 250.00
wsandman28 posts ante 250.00
gdref posts ante 250.00
AAngelov180 posts ante 250.00
Hashmd posts ante 250.00
ViperCity253 posts ante 250.00
Out Of Range posts ante 250.00
Hashmd posts the small blind 1250.00
ViperCity253 posts the big blind 2500.00
*** HOLE CARDS ***
Main pot 2250.00
Dealt to xelnaga90 [3c 2s]
Out Of Range folds
r23y folds
xelnaga90 folds
Twizy folds
wsandman28 folds
gdref folds
AAngelov180 folds
Hashmd folds
ViperCity253 does not show
*** SUMMARY ***
Total pot 6000.00
Seat 1: r23y folded on the Pre-Flop and did not bet
Seat 2: xelnaga90 folded on the Pre-Flop and did not bet
Seat 3: Twizy folded on the Pre-Flop and did not bet
Seat 4: wsandman28 folded on the Pre-Flop and did not bet
Seat 5: gdref folded on the Pre-Flop and did not bet
Seat 6: AAngelov180 (button) folded on the Pre-Flop
Seat 7: Hashmd (small blind) folded on the Pre-Flop
Seat 8: ViperCity253 did not show and won 6000.00
Seat 9: Out Of Range folded on the Pre-Flop and did not bet

Game Hand #1156450996 - Tournament #26537363 - Holdem(No Limit) - Level 6 (1250.00/2500.00)- 2022/03/15 01:05:25 UTC
Table '7' 9-max Seat #7 is the button
Seat 1: r23y (29750.00)
Seat 2: xelnaga90 (42400.00)
Seat 4: wsandman28 (28487.00)
Seat 5: gdref (30450.00)
Seat 6: AAngelov180 (82347.00)
Seat 7: Hashmd (20949.00) is sitting out
Seat 8: ViperCity253 (118979.00)
Seat 9: Out Of Range (29750.00)
r23y posts ante 250.00
xelnaga90 posts ante 250.00
wsandman28 posts ante 250.00
gdref posts ante 250.00
AAngelov180 posts ante 250.00
Hashmd posts ante 250.00
ViperCity253 posts ante 250.00
Out Of Range posts ante 250.00
ViperCity253 posts the small blind 1250.00
Out Of Range posts the big blind 2500.00
*** HOLE CARDS ***
Main pot 2000.00
Dealt to xelnaga90 [8c Ts]
r23y folds
xelnaga90 folds
wsandman28 folds
gdref calls 2500.00
AAngelov180 raises 5000.00 to 5000.00
Hashmd folds
ViperCity253 folds
Out Of Range raises 27000.00 to 29500.00 and is all-in
gdref raises 27700.00 to 30200.00 and is all-in
AAngelov180 folds
Uncalled bet (700.00) returned to gdref
*** FLOP *** [2d 9s 7s]
Main pot 67250.00
*** TURN *** [2d 9s 7s] [6s]
Main pot 67250.00
*** RIVER *** [2d 9s 7s 6s] [7d]
Main pot 67250.00
*** SHOW DOWN ***
Main pot 67250.00
gdref shows [Ad 9d] (two pair, Nines and Sevens [9s 9d 7s 7d Ad])
Out Of Range shows [Ks Ah] (a pair of Sevens [7s 7d Ah Ks 9s])
gdref collected 67250.00 from main pot
*** SUMMARY ***
Total pot 67250.00
Board [2d 9s 7s 6s 7d]
Seat 1: r23y folded on the Pre-Flop and did not bet
Seat 2: xelnaga90 folded on the Pre-Flop and did not bet
Seat 4: wsandman28 folded on the Pre-Flop and did not bet
Seat 5: gdref showed [Ad 9d] and won 67250.00 with two pair, Nines and Sevens [9s 9d 7s 7d Ad]
Seat 6: AAngelov180 folded on the Pre-Flop and did not bet
Seat 7: Hashmd (button) folded on the Pre-Flop
Seat 8: ViperCity253 (small blind) folded on the Pre-Flop
Seat 9: Out Of Range (big blind) showed [Ks Ah] and lost with a pair of Sevens [7s 7d Ah Ks 9s]

Game Hand #1156452615 - Tournament #26537363 - Holdem(No Limit) - Level 6 (1250.00/2500.00)- 2022/03/15 01:06:30 UTC
Table '7' 9-max Seat #8 is the button
Seat 1: r23y (29500.00)
Seat 2: xelnaga90 (42150.00)
Seat 3: hanio75 (30000.00)
Seat 4: wsandman28 (28237.00)
Seat 5: gdref (67950.00)
Seat 6: AAngelov180 (77097.00)
Seat 7: Hashmd (20699.00) is sitting out
Seat 8: ViperCity253 (117479.00)
r23y posts ante 250.00
xelnaga90 posts ante 250.00
hanio75 posts ante 250.00
wsandman28 posts ante 250.00
gdref posts ante 250.00
AAngelov180 posts ante 250.00
Hashmd posts ante 250.00
ViperCity253 posts ante 250.00
r23y posts the big blind 2500.00
*** HOLE CARDS ***
Main pot 2000.00
Dealt to xelnaga90 [5s Ts]
xelnaga90 folds
hanio75 folds
wsandman28 folds
gdref raises 6000.00 to 6000.00
AAngelov180 folds
Hashmd folds
ViperCity253 folds
r23y raises 26750.00 to 29250.00 and is all-in
gdref calls 23250.00
*** FLOP *** [Th Kd 6h]
Main pot 60500.00
*** TURN *** [Th Kd 6h] [8d]
Main pot 60500.00
*** RIVER *** [Th Kd 6h 8d] [5h]
Main pot 60500.00
*** SHOW DOWN ***
Main pot 60500.00
r23y shows [Tc Td] (three of a kind, Set of Tens [Th Td Tc Kd 8d])
gdref shows [Ks Ad] (a pair of Kings [Ks Kd Ad Th 8d])
r23y collected 60500.00 from main pot
*** SUMMARY ***
Total pot 60500.00
Board [Th Kd 6h 8d 5h]
Seat 1: r23y (big blind) showed [Tc Td] and won 60500.00 with three of a kind, Set of Tens [Th Td Tc Kd 8d]
Seat 2: xelnaga90 folded on the Pre-Flop and did not bet
Seat 3: hanio75 folded on the Pre-Flop and did not bet
Seat 4: wsandman28 folded on the Pre-Flop and did not bet
Seat 5: gdref showed [Ks Ad] and lost with a pair of Kings [Ks Kd Ad Th 8d]
Seat 6: AAngelov180 folded on the Pre-Flop and did not bet
Seat 7: Hashmd folded on the Pre-Flop and did not bet
Seat 8: ViperCity253 (button) folded on the Pre-Flop

Game Hand #1156453711 - Tournament #26537363 - Holdem(No Limit) - Level 6 (1250.00/2500.00)- 2022/03/15 01:07:12 UTC
Table '7' 9-max Seat #9 is the button
Seat 1: r23y (60500.00)
Seat 2: xelnaga90 (41900.00)
Seat 3: hanio75 (29750.00)
Seat 4: wsandman28 (27987.00)
Seat 5: gdref (38450.00)
Seat 6: AAngelov180 (76847.00)
Seat 7: Hashmd (20449.00) is sitting out
Seat 8: ViperCity253 (117229.00)
r23y posts ante 250.00
xelnaga90 posts ante 250.00
hanio75 posts ante 250.00
wsandman28 posts ante 250.00
gdref posts ante 250.00
AAngelov180 posts ante 250.00
Hashmd posts ante 250.00
ViperCity253 posts ante 250.00
r23y posts the small blind 1250.00
xelnaga90 posts the big blind 2500.00
*** HOLE CARDS ***
Main pot 2000.00
Dealt to xelnaga90 [Jd 4c]
hanio75 folds
wsandman28 calls 2500.00
gdref folds
AAngelov180 folds
Hashmd folds
ViperCity253 folds
r23y calls 1250.00
xelnaga90 checks
*** FLOP *** [4h 3c Ad]
Main pot 9500.00
r23y checks
xelnaga90 checks
wsandman28 bets 2500.00
r23y calls 2500.00
xelnaga90 calls 2500.00
*** TURN *** [4h 3c Ad] [Qc]
Main pot 17000.00
r23y checks
xelnaga90 checks
wsandman28 bets 2500.00
r23y calls 2500.00
xelnaga90 folds
*** RIVER *** [4h 3c Ad Qc] [2c]
Main pot 22000.00
r23y checks
wsandman28 bets 16500.00
r23y folds
Uncalled bet (16500.00) returned to wsandman28
wsandman28 does not show
*** SUMMARY ***
Total pot 22000.00
Board [4h 3c Ad Qc 2c]
Seat 1: r23y (small blind) folded on the River
Seat 2: xelnaga90 (big blind) folded on the Turn
Seat 3: hanio75 folded on the Pre-Flop and did not bet
Seat 4: wsandman28 did not show and won 22000.00
Seat 5: gdref folded on the Pre-Flop and did not bet
Seat 6: AAngelov180 folded on the Pre-Flop and did not bet
Seat 7: Hashmd folded on the Pre-Flop and did not bet
Seat 8: ViperCity253 folded on the Pre-Flop and did not bet

Game Hand #1156456246 - Tournament #26537363 - Holdem(No Limit) - Level 7 (1500.00/3000.00)- 2022/03/15 01:08:56 UTC
Table '7' 9-max Seat #1 is the button
Seat 1: r23y (52750.00)
Seat 2: xelnaga90 (36650.00)
Seat 3: hanio75 (29500.00)
Seat 4: wsandman28 (42237.00)
Seat 5: gdref (38200.00)
Seat 6: AAngelov180 (76597.00)
Seat 7: Hashmd (20199.00) is sitting out
Seat 8: ViperCity253 (116979.00)
r23y posts ante 300.00
xelnaga90 posts ante 300.00
hanio75 posts ante 300.00
wsandman28 posts ante 300.00
gdref posts ante 300.00
AAngelov180 posts ante 300.00
Hashmd posts ante 300.00
ViperCity253 posts ante 300.00
xelnaga90 posts the small blind 1500.00
hanio75 posts the big blind 3000.00
*** HOLE CARDS ***
Main pot 2400.00
Dealt to xelnaga90 [Qd 6h]
wsandman28 folds
gdref folds
AAngelov180 folds
Hashmd folds
ViperCity253 folds
r23y folds
xelnaga90 folds
hanio75 does not show
*** SUMMARY ***
Total pot 6900.00
Seat 1: r23y (button) folded on the Pre-Flop
Seat 2: xelnaga90 (small blind) folded on the Pre-Flop
Seat 3: hanio75 did not show and won 6900.00
Seat 4: wsandman28 folded on the Pre-Flop and did not bet
Seat 5: gdref folded on the Pre-Flop and did not bet
Seat 6: AAngelov180 folded on the Pre-Flop and did not bet
Seat 7: Hashmd folded on the Pre-Flop and did not bet
Seat 8: ViperCity253 folded on the Pre-Flop and did not bet

Game Hand #1156457236 - Tournament #26537363 - Holdem(No Limit) - Level 7 (1500.00/3000.00)- 2022/03/15 01:09:34 UTC
Table '7' 9-max Seat #2 is the button
Seat 1: r23y (52450.00)
Seat 2: xelnaga90 (34850.00)
Seat 3: hanio75 (33100.00)
Seat 4: wsandman28 (41937.00)
Seat 5: gdref (37900.00)
Seat 6: AAngelov180 (76297.00)
Seat 7: Hashmd (19899.00) is sitting out
Seat 8: ViperCity253 (116679.00)
r23y posts ante 300.00
xelnaga90 posts ante 300.00
hanio75 posts ante 300.00
wsandman28 posts ante 300.00
gdref posts ante 300.00
AAngelov180 posts ante 300.00
Hashmd posts ante 300.00
ViperCity253 posts ante 300.00
hanio75 posts the small blind 1500.00
wsandman28 posts the big blind 3000.00
*** HOLE CARDS ***
Main pot 2400.00
Dealt to xelnaga90 [Ac 7c]
gdref folds
AAngelov180 folds
Hashmd folds
ViperCity253 raises 6600.00 to 6600.00
r23y calls 6600.00
xelnaga90 raises 34550.00 to 34550.00 and is all-in
hanio75 calls 31300.00 and is all-in
wsandman28 folds
ViperCity253 folds
r23y folds
Uncalled bet (1750.00) returned to xelnaga90
*** FLOP *** [3s Jc 8h]
Main pot 84200.00
*** TURN *** [3s Jc 8h] [2d]
Main pot 84200.00
*** RIVER *** [3s Jc 8h 2d] [Qd]
Main pot 84200.00
*** SHOW DOWN ***
Main pot 84200.00
xelnaga90 shows [Ac 7c] (a high card, Ace high [Ac Qd Jc 8h 7c])
hanio75 shows [Js Jd] (three of a kind, Set of Jacks [Js Jd Jc Qd 8h])
hanio75 collected 84200.00 from main pot
*** SUMMARY ***
Total pot 84200.00
Board [3s Jc 8h 2d Qd]
Seat 1: r23y folded on the Pre-Flop and did not bet
Seat 2: xelnaga90 (button) showed [Ac 7c] and lost with a high card, Ace high [Ac Qd Jc 8h 7c]
Seat 3: hanio75 (small blind) showed [Js Jd] and won 84200.00 with three of a kind, Set of Jacks [Js Jd Jc Qd 8h]
Seat 4: wsandman28 (big blind) folded on the Pre-Flop
Seat 5: gdref folded on the Pre-Flop and did not bet
Seat 6: AAngelov180 folded on the Pre-Flop and did not bet
Seat 7: Hashmd folded on the Pre-Flop and did not bet
Seat 8: ViperCity253 folded on the Pre-Flop and did not bet

Game Hand #1156458795 - Tournament #26537363 - Holdem(No Limit) - Level 7 (1500.00/3000.00)- 2022/03/15 01:10:35 UTC
Table '7' 9-max Seat #3 is the button
Seat 1: r23y (45550.00)
Seat 2: xelnaga90 (1750.00)
Seat 3: hanio75 (84200.00)
Seat 4: wsandman28 (38637.00)
Seat 5: gdref (37600.00)
Seat 6: AAngelov180 (75997.00)
Seat 7: Hashmd (19599.00) is sitting out
Seat 8: ViperCity253 (109779.00)
Seat 9: jimmoore1 (45300.00)
r23y posts ante 300.00
xelnaga90 posts ante 300.00
hanio75 posts ante 300.00
wsandman28 posts ante 300.00
gdref posts ante 300.00
AAngelov180 posts ante 300.00
Hashmd posts ante 300.00
ViperCity253 posts ante 300.00
jimmoore1 posts ante 300.00
wsandman28 posts the small blind 1500.00
gdref posts the big blind 3000.00
*** HOLE CARDS ***
Main pot 2700.00
Dealt to xelnaga90 [5c 9d]
AAngelov180 folds
Hashmd folds
ViperCity253 folds
jimmoore1 folds
r23y folds
xelnaga90 calls 1450.00 and is all-in
hanio75 folds
wsandman28 calls 1500.00
gdref raises 10150.00 to 13150.00
wsandman28 folds
Uncalled bet (13250.00) returned to gdref
*** FLOP *** [Ad 6s Kh]
Main pot 7050.00
*** TURN *** [Ad 6s Kh] [Jh]
Main pot 7050.00
*** RIVER *** [Ad 6s Kh Jh] [Td]
Main pot 7050.00
*** SHOW DOWN ***
Main pot 7050.00
xelnaga90 shows [5c 9d] (a high card, Ace high [Ad Kh Jh Td 9d])
gdref shows [Ah Qh] (a straight, Ace high [Ah Kh Qh Jh Td])
gdref collected 7050.00 from main pot
*** SUMMARY ***
Total pot 7050.00
Board [Ad 6s Kh Jh Td]
Seat 1: r23y folded on the Pre-Flop and did not bet
Seat 2: xelnaga90 showed [5c 9d] and lost with a high card, Ace high [Ad Kh Jh Td 9d]
Seat 3: hanio75 (button) folded on the Pre-Flop
Seat 4: wsandman28 (small blind) folded on the Pre-Flop
Seat 5: gdref (big blind) showed [Ah Qh] and won 7050.00 with a straight, Ace high [Ah Kh Qh Jh Td]
Seat 6: AAngelov180 folded on the Pre-Flop and did not bet
Seat 7: Hashmd folded on the Pre-Flop and did not bet
Seat 8: ViperCity253 folded on the Pre-Flop and did not bet
Seat 9: jimmoore1 folded on the Pre-Flop and did not bet

