Hand #1156463464 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:13:42 UTC
Hartwell 6-max Seat #3 is the button
Seat 1: Tembolo78 ($4.20)
Seat 2: MasterBlueXXX ($5.00)
Seat 3: Navy1985 ($5.14)
Seat 4: djm9163 ($9.45)
Seat 5: xelnaga90 ($5.00)
Seat 6: DifferenHaX ($21.70)
djm9163 posts the small blind $0.02
xelnaga90 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [8c 7c 2c 6c]
DifferenHaX raises $0.13 to $0.13
Tembolo78 folds
MasterBlueXXX folds
Navy1985 calls $0.13
djm9163 folds
xelnaga90 calls $0.08
*** FLOP *** [3c Js 4c]
Main pot $0.39 | Rake $0.02
xelnaga90 checks
DifferenHaX bets $0.21
Navy1985 calls $0.21
xelnaga90 calls $0.21
*** TURN *** [3c Js 4c] [Kh]
Main pot $0.99 | Rake $0.05
xelnaga90 checks
DifferenHaX checks
Navy1985 bets $0.78
xelnaga90 folds
DifferenHaX calls $0.78
*** RIVER *** [3c Js 4c Kh] [Td]
Main pot $2.47 | Rake $0.13
DifferenHaX bets $0.75
Navy1985 calls $0.75
*** SHOW DOWN ***
Main pot $3.90 | Rake $0.20
Navy1985 shows [7h 2s Kc Kd] (three of a kind, Set of Kings [Kh Kd Kc Js Td])
DifferenHaX shows [Qd 7d Ac Qs] (a straight, Ace high [Ac Kh Qd Js Td])
DifferenHaX collected $3.90 from main pot
*** SUMMARY ***
Total pot $3.90 | Rake $0.14 | JP Fee $0.06
Board [3c Js 4c Kh Td]
Seat 1: Tembolo78 folded on the Pre-Flop and did not bet
Seat 2: MasterBlueXXX folded on the Pre-Flop and did not bet
Seat 3: Navy1985 (button) showed [7h 2s Kc Kd] and lost with three of a kind, Set of Kings [Kh Kd Kc Js Td]
Seat 4: djm9163 (small blind) folded on the Pre-Flop
Seat 5: xelnaga90 (big blind) folded on the Turn
Seat 6: DifferenHaX showed [Qd 7d Ac Qs] and won $3.90 with a straight, Ace high [Ac Kh Qd Js Td]

Hand #1156467127 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:16:07 UTC
Hartwell 6-max Seat #4 is the button
Seat 1: Tembolo78 ($4.20)
Seat 2: MasterBlueXXX ($5.00)
Seat 3: Navy1985 ($3.27)
Seat 4: djm9163 ($9.43)
Seat 5: xelnaga90 ($4.66)
Seat 6: DifferenHaX ($23.73)
xelnaga90 posts the small blind $0.02
DifferenHaX posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [2c Kc 6h 2h]
Tembolo78 raises $0.17 to $0.17
MasterBlueXXX folds
Navy1985 folds
djm9163 calls $0.17
xelnaga90 folds
DifferenHaX calls $0.12
*** FLOP *** [5d 2s 6d]
Main pot $0.51 | Rake $0.02
DifferenHaX checks
Tembolo78 bets $0.37
djm9163 folds
DifferenHaX folds
Uncalled bet ($0.37) returned to Tembolo78
Tembolo78 does not show
*** SUMMARY ***
Total pot $0.51 | Rake $0.02
Board [5d 2s 6d]
Seat 1: Tembolo78 did not show and won $0.51
Seat 2: MasterBlueXXX folded on the Pre-Flop and did not bet
Seat 3: Navy1985 folded on the Pre-Flop and did not bet
Seat 4: djm9163 (button) folded on the Flop
Seat 5: xelnaga90 (small blind) folded on the Pre-Flop
Seat 6: DifferenHaX (big blind) folded on the Flop

Hand #1156469080 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:17:26 UTC
Hartwell 6-max Seat #5 is the button
Seat 1: Tembolo78 ($4.54)
Seat 2: MasterBlueXXX ($5.00)
Seat 3: Navy1985 ($3.27)
Seat 4: djm9163 ($9.26)
Seat 5: xelnaga90 ($5.00)
Seat 6: DifferenHaX ($23.56)
DifferenHaX posts the small blind $0.02
Tembolo78 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [5s 9s 9d 9c]
MasterBlueXXX folds
Navy1985 folds
djm9163 folds
xelnaga90 raises $0.12 to $0.12
DifferenHaX folds
Tembolo78 folds
Uncalled bet ($0.07) returned to xelnaga90
xelnaga90 does not show
*** SUMMARY ***
Total pot $0.12
Seat 1: Tembolo78 (big blind) folded on the Pre-Flop
Seat 2: MasterBlueXXX folded on the Pre-Flop and did not bet
Seat 3: Navy1985 folded on the Pre-Flop and did not bet
Seat 4: djm9163 folded on the Pre-Flop and did not bet
Seat 5: xelnaga90 did not show and won $0.12
Seat 6: DifferenHaX (small blind) folded on the Pre-Flop

Hand #1156469938 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:18:02 UTC
Hartwell 6-max Seat #6 is the button
Seat 1: Tembolo78 ($4.49)
Seat 2: MasterBlueXXX ($5.00)
Seat 3: Navy1985 ($3.27)
Seat 4: djm9163 ($9.26)
Seat 5: xelnaga90 ($5.07)
Seat 6: DifferenHaX ($23.54)
Tembolo78 posts the small blind $0.02
MasterBlueXXX posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [4s 8d 9d Kc]
Navy1985 folds
djm9163 raises $0.17 to $0.17
xelnaga90 folds
DifferenHaX calls $0.17
Tembolo78 calls $0.15
MasterBlueXXX folds
*** FLOP *** [5c Ks Ts]
Main pot $0.54 | Rake $0.02
Tembolo78 checks
djm9163 bets $0.30
DifferenHaX folds
Tembolo78 folds
Uncalled bet ($0.30) returned to djm9163
djm9163 does not show
*** SUMMARY ***
Total pot $0.54 | Rake $0.02
Board [5c Ks Ts]
Seat 1: Tembolo78 (small blind) folded on the Flop
Seat 2: MasterBlueXXX (big blind) folded on the Pre-Flop
Seat 3: Navy1985 folded on the Pre-Flop and did not bet
Seat 4: djm9163 did not show and won $0.54
Seat 5: xelnaga90 folded on the Pre-Flop and did not bet
Seat 6: DifferenHaX (button) folded on the Flop

Hand #1156472172 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:19:31 UTC
Hartwell 6-max Seat #1 is the button
Seat 1: Tembolo78 ($4.32)
Seat 2: MasterBlueXXX ($4.95)
Seat 3: Navy1985 ($3.27)
Seat 4: djm9163 ($9.63)
Seat 5: xelnaga90 ($5.07)
Seat 6: DifferenHaX ($23.37)
MasterBlueXXX posts the small blind $0.02
Navy1985 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [9d Qh 5s Ac]
djm9163 calls $0.05
xelnaga90 calls $0.05
DifferenHaX folds
Tembolo78 folds
MasterBlueXXX folds
Navy1985 checks
*** FLOP *** [6s 5c 9c]
Main pot $0.17 | Rake $0.00
Navy1985 checks
djm9163 checks
xelnaga90 bets $0.07
Navy1985 folds
djm9163 folds
Uncalled bet ($0.07) returned to xelnaga90
xelnaga90 does not show
*** SUMMARY ***
Total pot $0.17
Board [6s 5c 9c]
Seat 1: Tembolo78 (button) folded on the Pre-Flop
Seat 2: MasterBlueXXX (small blind) folded on the Pre-Flop
Seat 3: Navy1985 (big blind) folded on the Flop
Seat 4: djm9163 folded on the Flop
Seat 5: xelnaga90 did not show and won $0.17
Seat 6: DifferenHaX folded on the Pre-Flop and did not bet

Hand #1156474359 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:20:58 UTC
Hartwell 6-max Seat #2 is the button
Seat 1: Tembolo78 ($4.32)
Seat 2: MasterBlueXXX ($4.93)
Seat 3: Navy1985 ($3.22)
Seat 4: djm9163 ($9.58)
Seat 5: xelnaga90 ($5.19)
Seat 6: DifferenHaX ($23.37)
Navy1985 posts the small blind $0.02
djm9163 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [9d 7c Qs 3h]
xelnaga90 folds
DifferenHaX folds
Tembolo78 raises $0.17 to $0.17
MasterBlueXXX folds
Navy1985 calls $0.15
djm9163 folds
*** FLOP *** [4s 9h Kh]
Main pot $0.38 | Rake $0.01
Navy1985 checks
Tembolo78 checks
*** TURN *** [4s 9h Kh] [Td]
Main pot $0.38 | Rake $0.01
Navy1985 checks
Tembolo78 checks
*** RIVER *** [4s 9h Kh Td] [5s]
Main pot $0.38 | Rake $0.01
Navy1985 checks
Tembolo78 checks
*** SHOW DOWN ***
Main pot $0.38 | Rake $0.01
Tembolo78 shows [3s 2d As 4c] (a pair of Fours [4s 4c As Kh Td])
Navy1985 shows [6h 2s Qc Qh] (a pair of Queens [Qh Qc Kh Td 9h])
Navy1985 collected $0.38 from main pot
*** SUMMARY ***
Total pot $0.38 | Rake $0.01
Board [4s 9h Kh Td 5s]
Seat 1: Tembolo78 showed [3s 2d As 4c] and lost with a pair of Fours [4s 4c As Kh Td]
Seat 2: MasterBlueXXX (button) folded on the Pre-Flop
Seat 3: Navy1985 (small blind) showed [6h 2s Qc Qh] and won $0.38 with a pair of Queens [Qh Qc Kh Td 9h]
Seat 4: djm9163 (big blind) folded on the Pre-Flop
Seat 5: xelnaga90 folded on the Pre-Flop and did not bet
Seat 6: DifferenHaX folded on the Pre-Flop and did not bet

Hand #1156476120 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:22:07 UTC
Hartwell 6-max Seat #3 is the button
Seat 1: Tembolo78 ($4.15)
Seat 2: MasterBlueXXX ($4.93)
Seat 3: Navy1985 ($3.43)
Seat 4: djm9163 ($9.53)
Seat 5: xelnaga90 ($5.19)
Seat 6: DifferenHaX ($23.37)
djm9163 posts the small blind $0.02
xelnaga90 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [5c 2h 3h 7c]
DifferenHaX folds
Tembolo78 raises $0.17 to $0.17
MasterBlueXXX folds
Navy1985 folds
djm9163 folds
xelnaga90 folds
Uncalled bet ($0.12) returned to Tembolo78
Tembolo78 does not show
*** SUMMARY ***
Total pot $0.12
Seat 1: Tembolo78 did not show and won $0.12
Seat 2: MasterBlueXXX folded on the Pre-Flop and did not bet
Seat 3: Navy1985 (button) folded on the Pre-Flop
Seat 4: djm9163 (small blind) folded on the Pre-Flop
Seat 5: xelnaga90 (big blind) folded on the Pre-Flop
Seat 6: DifferenHaX folded on the Pre-Flop and did not bet

Hand #1156476897 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:22:38 UTC
Hartwell 6-max Seat #4 is the button
Seat 1: Tembolo78 ($4.22)
Seat 2: MasterBlueXXX ($4.93)
Seat 3: Navy1985 ($3.43)
Seat 4: djm9163 ($9.51)
Seat 5: xelnaga90 ($5.14)
Seat 6: DifferenHaX ($23.37)
xelnaga90 posts the small blind $0.02
DifferenHaX posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [2h 6c 9h 2d]
Tembolo78 folds
MasterBlueXXX calls $0.05
Navy1985 calls $0.05
djm9163 folds
xelnaga90 calls $0.03
DifferenHaX raises $0.17 to $0.22
MasterBlueXXX calls $0.17
Navy1985 calls $0.17
xelnaga90 folds
*** FLOP *** [6h Qh 5c]
Main pot $0.68 | Rake $0.03
DifferenHaX bets $0.25
MasterBlueXXX calls $0.25
Navy1985 folds
*** TURN *** [6h Qh 5c] [Ts]
Main pot $1.15 | Rake $0.06
DifferenHaX bets $0.47
MasterBlueXXX calls $0.47
*** RIVER *** [6h Qh 5c Ts] [3d]
Main pot $2.05 | Rake $0.10
DifferenHaX bets $1.00
MasterBlueXXX calls $1.00
*** SHOW DOWN ***
Main pot $3.95 | Rake $0.20
MasterBlueXXX shows [8c Qc Jc Th] (two pair, Queens and Tens [Qh Qc Ts Th 6h])
DifferenHaX shows [Ks Td Kc 7h] (a pair of Kings [Ks Kc Qh Ts 6h])
MasterBlueXXX collected $3.95 from main pot
*** SUMMARY ***
Total pot $3.95 | Rake $0.14 | JP Fee $0.06
Board [6h Qh 5c Ts 3d]
Seat 1: Tembolo78 folded on the Pre-Flop and did not bet
Seat 2: MasterBlueXXX showed [8c Qc Jc Th] and won $3.95 with two pair, Queens and Tens [Qh Qc Ts Th 6h]
Seat 3: Navy1985 folded on the Flop
Seat 4: djm9163 (button) folded on the Pre-Flop
Seat 5: xelnaga90 (small blind) folded on the Pre-Flop
Seat 6: DifferenHaX (big blind) showed [Ks Td Kc 7h] and lost with a pair of Kings [Ks Kc Qh Ts 6h]

Hand #1156480096 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:24:47 UTC
Hartwell 6-max Seat #5 is the button
Seat 1: Tembolo78 ($4.22)
Seat 2: MasterBlueXXX ($6.94)
Seat 3: Navy1985 ($3.21)
Seat 4: djm9163 ($9.51)
Seat 5: xelnaga90 ($5.09)
Seat 6: DifferenHaX ($21.43)
DifferenHaX posts the small blind $0.02
Tembolo78 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [3d Qc 9h Ac]
MasterBlueXXX calls $0.05
Navy1985 calls $0.05
djm9163 calls $0.05
xelnaga90 raises $0.12 to $0.12
DifferenHaX folds
Tembolo78 folds
MasterBlueXXX calls $0.07
Navy1985 calls $0.07
djm9163 calls $0.07
*** FLOP *** [8s 5s Js]
Main pot $0.53 | Rake $0.02
MasterBlueXXX checks
Navy1985 bets $0.41
djm9163 folds
xelnaga90 folds
MasterBlueXXX folds
Uncalled bet ($0.41) returned to Navy1985
Navy1985 does not show
*** SUMMARY ***
Total pot $0.53 | Rake $0.02
Board [8s 5s Js]
Seat 1: Tembolo78 (big blind) folded on the Pre-Flop
Seat 2: MasterBlueXXX folded on the Flop
Seat 3: Navy1985 did not show and won $0.53
Seat 4: djm9163 folded on the Flop
Seat 5: xelnaga90 (button) folded on the Flop
Seat 6: DifferenHaX (small blind) folded on the Pre-Flop

Hand #1156482499 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:26:26 UTC
Hartwell 6-max Seat #6 is the button
Seat 1: Tembolo78 ($4.17)
Seat 2: MasterBlueXXX ($6.82)
Seat 3: Navy1985 ($3.62)
Seat 4: djm9163 ($9.39)
Seat 5: xelnaga90 ($4.97)
Seat 6: DifferenHaX ($21.41)
Tembolo78 posts the small blind $0.02
MasterBlueXXX posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [Th Qd Tc 9h]
Navy1985 calls $0.05
djm9163 folds
xelnaga90 raises $0.17 to $0.17
DifferenHaX folds
Tembolo78 raises $0.59 to $0.61
MasterBlueXXX folds
Navy1985 raises $2.00 to $2.05
xelnaga90 folds
Tembolo78 raises $3.56 to $4.17 and is all-in
Navy1985 calls $1.57 and is all-in
Uncalled bet ($0.55) returned to Tembolo78
*** FLOP *** [9c Kc 9s]
Main pot $7.09 | Rake $0.37
*** TURN *** [9c Kc 9s] [8d]
Main pot $7.09 | Rake $0.37
*** RIVER *** [9c Kc 9s 8d] [4d]
Main pot $7.09 | Rake $0.37
*** SHOW DOWN ***
Main pot $7.09 | Rake $0.37
Tembolo78 shows [As Qc Kd 4c] (two pair, Kings and Nines [Kd Kc 9s 9c As])
Navy1985 shows [Ks Ac Jd Ad] (two pair, Aces and Nines [Ad Ac 9s 9c Kc])
Navy1985 collected $7.09 from main pot
*** SUMMARY ***
Total pot $7.09 | Rake $0.25 | JP Fee $0.12
Board [9c Kc 9s 8d 4d]
Seat 1: Tembolo78 (small blind) showed [As Qc Kd 4c] and lost with two pair, Kings and Nines [Kd Kc 9s 9c As]
Seat 2: MasterBlueXXX (big blind) folded on the Pre-Flop
Seat 3: Navy1985 showed [Ks Ac Jd Ad] and won $7.09 with two pair, Aces and Nines [Ad Ac 9s 9c Kc]
Seat 4: djm9163 folded on the Pre-Flop and did not bet
Seat 5: xelnaga90 folded on the Pre-Flop and did not bet
Seat 6: DifferenHaX (button) folded on the Pre-Flop

Hand #1156484853 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:28:01 UTC
Hartwell 6-max Seat #1 is the button
Seat 2: MasterBlueXXX ($6.77)
Seat 3: Navy1985 ($7.09)
Seat 4: djm9163 ($9.39)
Seat 5: xelnaga90 ($4.80)
Seat 6: DifferenHaX ($21.41)
MasterBlueXXX posts the small blind $0.02
Navy1985 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [4s 2s 4c Qc]
djm9163 folds
xelnaga90 calls $0.05
DifferenHaX folds
MasterBlueXXX folds
Navy1985 checks
*** FLOP *** [Ah 7s 8h]
Main pot $0.12 | Rake $0.00
Navy1985 checks
xelnaga90 checks
*** TURN *** [Ah 7s 8h] [6s]
Main pot $0.12 | Rake $0.00
Navy1985 checks
xelnaga90 checks
*** RIVER *** [Ah 7s 8h 6s] [9h]
Main pot $0.12 | Rake $0.00
Navy1985 checks
xelnaga90 checks
*** SHOW DOWN ***
Main pot $0.12 | Rake $0.00
Navy1985 shows [Ad 6d Ts Qh] (a straight, Ten high [Ts 9h 8h 7s 6d])
xelnaga90 shows [4s 2s 4c Qc] (a pair of Fours [4s 4c Ah 9h 8h])
Navy1985 collected $0.12 from main pot
*** SUMMARY ***
Total pot $0.12
Board [Ah 7s 8h 6s 9h]
Seat 2: MasterBlueXXX (small blind) folded on the Pre-Flop
Seat 3: Navy1985 (big blind) showed [Ad 6d Ts Qh] and won $0.12 with a straight, Ten high [Ts 9h 8h 7s 6d]
Seat 4: djm9163 folded on the Pre-Flop and did not bet
Seat 5: xelnaga90 showed [4s 2s 4c Qc] and lost with a pair of Fours [4s 4c Ah 9h 8h]
Seat 6: DifferenHaX folded on the Pre-Flop and did not bet

Hand #1156486476 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:29:08 UTC
Hartwell 6-max Seat #2 is the button
Seat 2: MasterBlueXXX ($6.75)
Seat 3: Navy1985 ($7.16)
Seat 4: djm9163 ($9.39)
Seat 5: xelnaga90 ($4.75)
Seat 6: DifferenHaX ($21.41)
Navy1985 posts the small blind $0.02
djm9163 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [3h 9h 4h Kd]
xelnaga90 folds
DifferenHaX folds
MasterBlueXXX folds
Navy1985 folds
djm9163 does not show
*** SUMMARY ***
Total pot $0.07
Seat 2: MasterBlueXXX (button) folded on the Pre-Flop
Seat 3: Navy1985 (small blind) folded on the Pre-Flop
Seat 4: djm9163 did not show and won $0.07
Seat 5: xelnaga90 folded on the Pre-Flop and did not bet
Seat 6: DifferenHaX folded on the Pre-Flop and did not bet

Hand #1156487390 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:29:46 UTC
Hartwell 6-max Seat #3 is the button
Seat 2: MasterBlueXXX ($6.75)
Seat 3: Navy1985 ($7.14)
Seat 4: djm9163 ($9.41)
Seat 5: xelnaga90 ($4.75)
Seat 6: DifferenHaX ($21.41)
djm9163 posts the small blind $0.02
xelnaga90 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [Qd 6h 2d 8c]
DifferenHaX folds
MasterBlueXXX calls $0.05
Navy1985 calls $0.05
djm9163 calls $0.03
xelnaga90 checks
*** FLOP *** [6s Td 5c]
Main pot $0.19 | Rake $0.01
djm9163 checks
xelnaga90 checks
MasterBlueXXX bets $0.20
Navy1985 folds
djm9163 calls $0.20
xelnaga90 folds
*** TURN *** [6s Td 5c] [As]
Main pot $0.57 | Rake $0.03
djm9163 checks
MasterBlueXXX bets $0.60
djm9163 calls $0.60
*** RIVER *** [6s Td 5c As] [Ah]
Main pot $1.71 | Rake $0.09
djm9163 checks
MasterBlueXXX bets $0.96
djm9163 calls $0.96
*** SHOW DOWN ***
Main pot $3.54 | Rake $0.18
MasterBlueXXX shows [Ts 3c 3d Th] (a full house, Tens full of Aces [Ts Th Td As Ah])
djm9163 shows [7s 4h Ad Jd] (three of a kind, Set of Aces [As Ah Ad Jd Td])
MasterBlueXXX collected $3.54 from main pot
*** SUMMARY ***
Total pot $3.54 | Rake $0.12 | JP Fee $0.06
Board [6s Td 5c As Ah]
Seat 2: MasterBlueXXX showed [Ts 3c 3d Th] and won $3.54 with a full house, Tens full of Aces [Ts Th Td As Ah]
Seat 3: Navy1985 (button) folded on the Flop
Seat 4: djm9163 (small blind) showed [7s 4h Ad Jd] and lost with three of a kind, Set of Aces [As Ah Ad Jd Td]
Seat 5: xelnaga90 (big blind) folded on the Flop
Seat 6: DifferenHaX folded on the Pre-Flop and did not bet

Hand #1156490251 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:31:43 UTC
Hartwell 6-max Seat #4 is the button
Seat 1: NoShrinkage ($2.00)
Seat 2: MasterBlueXXX ($8.48)
Seat 3: Navy1985 ($7.09)
Seat 4: djm9163 ($7.60)
Seat 5: xelnaga90 ($4.70)
Seat 6: DifferenHaX ($21.41)
xelnaga90 posts the small blind $0.02
DifferenHaX posts the big blind $0.05
NoShrinkage posts $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [4c 9d 2c As]
NoShrinkage checks
MasterBlueXXX folds
Navy1985 calls $0.05
djm9163 raises $0.27 to $0.27
xelnaga90 folds
DifferenHaX folds
NoShrinkage folds
Navy1985 calls $0.22
*** FLOP *** [7s 4s 6d]
Main pot $0.63 | Rake $0.03
Navy1985 checks
djm9163 checks
*** TURN *** [7s 4s 6d] [8h]
Main pot $0.63 | Rake $0.03
Navy1985 bets $0.66
djm9163 folds
Uncalled bet ($0.66) returned to Navy1985
Navy1985 does not show
*** SUMMARY ***
Total pot $0.63 | Rake $0.03
Board [7s 4s 6d 8h]
Seat 1: NoShrinkage folded on the Pre-Flop and did not bet
Seat 2: MasterBlueXXX folded on the Pre-Flop and did not bet
Seat 3: Navy1985 did not show and won $0.63
Seat 4: djm9163 (button) folded on the Turn
Seat 5: xelnaga90 (small blind) folded on the Pre-Flop
Seat 6: DifferenHaX (big blind) folded on the Pre-Flop

Hand #1156491847 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:32:50 UTC
Hartwell 6-max Seat #5 is the button
Seat 1: NoShrinkage ($1.95)
Seat 2: MasterBlueXXX ($8.48)
Seat 3: Navy1985 ($7.45)
Seat 4: djm9163 ($7.33)
Seat 5: xelnaga90 ($4.68)
Seat 6: DifferenHaX ($21.36)
DifferenHaX posts the small blind $0.02
NoShrinkage posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [9d 8s 8h 2c]
MasterBlueXXX folds
Navy1985 calls $0.05
djm9163 calls $0.05
xelnaga90 calls $0.05
DifferenHaX raises $0.11 to $0.13
NoShrinkage calls $0.08
Navy1985 calls $0.08
djm9163 calls $0.08
xelnaga90 calls $0.08
*** FLOP *** [9s 5h 5c]
Main pot $0.62 | Rake $0.03
DifferenHaX checks
NoShrinkage bets $0.21
Navy1985 folds
djm9163 folds
xelnaga90 calls $0.21
DifferenHaX calls $0.21
*** TURN *** [9s 5h 5c] [9c]
Main pot $1.22 | Rake $0.06
DifferenHaX checks
NoShrinkage checks
xelnaga90 bets $0.38
DifferenHaX folds
NoShrinkage folds
Uncalled bet ($0.38) returned to xelnaga90
xelnaga90 does not show
*** SUMMARY ***
Total pot $1.22 | Rake $0.04 | JP Fee $0.02
Board [9s 5h 5c 9c]
Seat 1: NoShrinkage (big blind) folded on the Turn
Seat 2: MasterBlueXXX folded on the Pre-Flop and did not bet
Seat 3: Navy1985 folded on the Flop
Seat 4: djm9163 folded on the Flop
Seat 5: xelnaga90 did not show and won $1.22
Seat 6: DifferenHaX (small blind) folded on the Turn

Hand #1156495505 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:35:20 UTC
Hartwell 6-max Seat #6 is the button
Seat 1: NoShrinkage ($1.61)
Seat 2: MasterBlueXXX ($8.48)
Seat 3: Navy1985 ($7.32)
Seat 5: xelnaga90 ($5.56)
Seat 6: DifferenHaX ($21.02)
NoShrinkage posts the small blind $0.02
MasterBlueXXX posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [4h Td Ad Jd]
Navy1985 calls $0.05
xelnaga90 calls $0.05
DifferenHaX raises $0.13 to $0.13
NoShrinkage folds
MasterBlueXXX calls $0.08
Navy1985 calls $0.08
xelnaga90 calls $0.08
*** FLOP *** [5d 4c 8h]
Main pot $0.52 | Rake $0.02
MasterBlueXXX checks
Navy1985 checks
xelnaga90 checks
DifferenHaX checks
*** TURN *** [5d 4c 8h] [7h]
Main pot $0.52 | Rake $0.02
MasterBlueXXX bets $0.54
Navy1985 folds
xelnaga90 folds
DifferenHaX folds
Uncalled bet ($0.54) returned to MasterBlueXXX
MasterBlueXXX does not show
*** SUMMARY ***
Total pot $0.52 | Rake $0.02
Board [5d 4c 8h 7h]
Seat 1: NoShrinkage (small blind) folded on the Pre-Flop
Seat 2: MasterBlueXXX did not show and won $0.52
Seat 3: Navy1985 folded on the Turn
Seat 5: xelnaga90 folded on the Turn
Seat 6: DifferenHaX (button) folded on the Turn

Hand #1156497571 - Omaha(Pot Limit) - $0.02/$0.05 - 2022/03/15 01:36:43 UTC
Hartwell 6-max Seat #1 is the button
Seat 1: NoShrinkage ($1.59)
Seat 2: MasterBlueXXX ($8.87)
Seat 3: Navy1985 ($7.19)
Seat 5: xelnaga90 ($5.43)
Seat 6: DifferenHaX ($20.89)
MasterBlueXXX posts the small blind $0.02
Navy1985 posts the big blind $0.05
*** HOLE CARDS ***
Dealt to xelnaga90 [Td As Qh 8d]
xelnaga90 raises $0.17 to $0.17
DifferenHaX folds
NoShrinkage folds
MasterBlueXXX calls $0.15
Navy1985 calls $0.12
*** FLOP *** [3c Ah 5d]
Main pot $0.49 | Rake $0.02
MasterBlueXXX bets $0.51
Navy1985 calls $0.51
xelnaga90 calls $0.51
*** TURN *** [3c Ah 5d] [Ts]
Main pot $1.94 | Rake $0.10
MasterBlueXXX bets $2.04
Navy1985 folds
xelnaga90 calls $2.04
*** RIVER *** [3c Ah 5d Ts] [2d]
Main pot $5.82 | Rake $0.30
MasterBlueXXX bets $6.12
xelnaga90 calls $2.71 and is all-in
Uncalled bet ($3.41) returned to MasterBlueXXX
*** SHOW DOWN ***
Main pot $10.97 | Rake $0.57
MasterBlueXXX shows [5h 2s 5s Th] (three of a kind, Set of Fives [5s 5h 5d Ah Ts])
xelnaga90 shows [Td As Qh 8d] (two pair, Aces and Tens [As Ah Ts Td 5d])
MasterBlueXXX collected $10.97 from main pot
*** SUMMARY ***
Total pot $10.97 | Rake $0.39 | JP Fee $0.18
Board [3c Ah 5d Ts 2d]
Seat 1: NoShrinkage (button) folded on the Pre-Flop
Seat 2: MasterBlueXXX (small blind) showed [5h 2s 5s Th] and won $10.97 with three of a kind, Set of Fives [5s 5h 5d Ah Ts]
Seat 3: Navy1985 (big blind) folded on the Turn
Seat 5: xelnaga90 showed [Td As Qh 8d] and lost with two pair, Aces and Tens [As Ah Ts Td 5d]
Seat 6: DifferenHaX folded on the Pre-Flop and did not bet

