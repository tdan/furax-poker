from typing import Union

import pytest

from furax_poker.fact import FactMetrics
from furax_poker.hand import Seat


@pytest.fixture
def hud_window_factory():
    class _window:
        is_visible = False
        is_closed = False
        seat = None
        x = 0
        y = 0

        def __init__(self, parent):
            self.parent = parent

        def refresh(self, seat: Seat, metrics: FactMetrics):
            self.is_closed = False
            self.seat = seat
            self.metrics = metrics

        def move(self, x, y):
            self.x = x
            self.y = y

        def close(self):
            self.is_closed = True
            self.parent.windows.remove(self)

        def show(self):
            self.is_visible = True

        def hide(self):
            self.is_visible = False

    class _window_factory:
        def __init__(self):
            self.windows = []

        def __call__(self, hud=None):
            self.windows.append(_window(self))
            return self.windows[-1]

        def get(self, seat_number_or_player_name: Union[int, str]):
            """Get window from player name or seat number."""
            if isinstance(seat_number_or_player_name, str):
                return next(
                    window
                    for window in self.windows
                    if window.seat.player.name == seat_number_or_player_name
                )
            return next(
                window
                for window in self.windows
                if window.seat.number == seat_number_or_player_name
            )

    return _window_factory()
