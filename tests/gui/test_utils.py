from furax_poker.gui.utils import (
    format_int,
    format_float,
    format_percent,
    format_big_number,
)


def test_format_int():
    assert format_int(None) == "-"
    assert format_int(None, if_none="NA") == "NA"
    assert format_int(32) == "32"
    assert format_int(32.2) == "32"
    assert format_int(0.32) == "0"


def test_format_float():
    assert format_float(None) == "-"
    assert format_float(None, if_none="NA") == "NA"
    assert format_float(32) == "32.0"
    assert format_float(32.22) == "32.2"


def test_format_percent():
    assert format_percent(None) == "-"
    assert format_percent(None, if_none="NA") == "NA"
    assert format_percent(0.32) == "32%"
    assert format_percent(0.322) == "32%"


def test_format_big_number():
    assert format_big_number(None) == "-"
    assert format_big_number(None, if_none="NA") == "NA"
    assert format_big_number(0.32) == "0"
    assert format_big_number(322) == "322"
    assert format_big_number(3221) == "3K"
    assert format_big_number(32210) == "32K"
    assert format_big_number(322100) == "322K"
    assert format_big_number(3221000) == "3M"
