from pathlib import Path
from unittest.mock import MagicMock

from PyQt5.QtCore import Qt
import pytest

from furax_poker.gui.settings_dialog import SettingsDialog
from furax_poker.hand import Room
from furax_poker.settings import ApplicationSettings, History, TimeScope


@pytest.fixture
def app():
    return MagicMock()


@pytest.fixture
def dialog(event_loop, session, settings, app):
    return SettingsDialog(None, app)


def test_create_dialog(dialog):
    assert bool(dialog)


def test_apply_change_existing_history(session, history, dialog, app):
    new_path = Path(history.path) / "subfolder"
    new_hero = "New Hero"
    assert history.path != str(new_path)
    assert history.hero != new_hero
    dialog._historyFolderLineEdit.setText(str(new_path))
    dialog._heroNicknameLineEdit.setText(new_hero)
    dialog.accept()
    app.tracker.restart.assert_called_once()

    # Reload history
    session.expire(history)
    history = session.query(History).one()
    assert history.path == str(new_path)
    assert history.hero == str(new_hero)


def test_apply_new_history(session, dialog, app, tmp_path):
    expected_path, expected_hero = str(tmp_path), "Hero"
    assert not session.query(History).all()
    dialog._historyFolderLineEdit.setText(str(tmp_path))
    dialog._heroNicknameLineEdit.setText(expected_hero)
    dialog.accept()
    app.tracker.restart.assert_called_once()

    history = session.query(History).one()
    assert history.path == expected_path
    assert history.hero == expected_hero


def test_apply_change_app_settings(history, session, settings, dialog, app):
    assert settings.autostart_hud
    assert settings.favorite_seat
    assert settings.hide_hud_when_window_not_active
    assert settings.follow_active_window
    assert settings.hero_time_scope == TimeScope.ALL_TIME
    assert settings.opponent_time_scope == TimeScope.ALL_TIME
    dialog._autostartHudCheckBox.setCheckState(Qt.Unchecked)
    dialog._favoriteSeatCheckBox.setCheckState(Qt.Unchecked)
    dialog._hideHudWhenWindowNotActiveCheckBox.setCheckState(Qt.Unchecked)
    dialog._followActiveWindowCheckBox.setCheckState(Qt.Unchecked)
    dialog._heroCurrentSessionRadioButton.setChecked(True)
    dialog._opponentHudCurrentSessionRadioButton.setChecked(True)
    dialog.accept()
    app.tracker.restart.assert_called_once()

    # Reload settings
    session.expire(settings)
    settings = session.query(ApplicationSettings).one()
    assert not settings.autostart_hud
    assert not settings.favorite_seat
    assert not settings.hide_hud_when_window_not_active
    assert not settings.follow_active_window
    assert settings.hero_time_scope == TimeScope.CURRENT_SESSION
    assert settings.opponent_time_scope == TimeScope.CURRENT_SESSION


def test_fill_form_history(history, dialog):
    assert dialog._historyFolderLineEdit.text() == history.path
    assert dialog._roomComboBox.currentData() == history.room.name
    assert dialog._heroNicknameLineEdit.text() == history.hero


def test_fill_form_new_history(dialog):
    assert dialog._historyFolderLineEdit.text() == ""
    assert dialog._roomComboBox.currentData() == Room.WINAMAX.name
    assert dialog._heroNicknameLineEdit.text() == ""


def test_fill_form_app_settings(settings, dialog):
    assert (
        dialog._autostartHudCheckBox.checkState() == Qt.Checked
    ) is settings.autostart_hud
    assert (
        dialog._favoriteSeatCheckBox.checkState() == Qt.Checked
    ) is settings.favorite_seat
    assert (
        dialog._hideHudWhenWindowNotActiveCheckBox.checkState() == Qt.Checked
    ) is settings.hide_hud_when_window_not_active
    assert (
        dialog._followActiveWindowCheckBox.checkState() == Qt.Checked
    ) is settings.follow_active_window
    assert dialog._heroAllTimeRadioButton.isChecked() is (
        settings.hero_time_scope == TimeScope.ALL_TIME
    )
    assert dialog._opponentHudAllTimeRadioButton.isChecked() is (
        settings.opponent_time_scope == TimeScope.ALL_TIME
    )
