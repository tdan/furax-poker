import pytest

from furax_poker.hand import SeatPosition


def test_seat_position_list_available():
    pos = SeatPosition.list_available(player_count=2)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND

    pos = SeatPosition.list_available(player_count=3)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=3, dead_small_blind=True)
    assert pos[0] is SeatPosition.BIG_BLIND
    assert pos[1] is SeatPosition.CUTOFF
    assert pos[2] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=4)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.CUTOFF
    assert pos[3] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=4, dead_small_blind=True)
    assert pos[0] is SeatPosition.BIG_BLIND
    assert pos[1] is SeatPosition.UTG_1
    assert pos[2] is SeatPosition.CUTOFF
    assert pos[3] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=5)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.UTG_1
    assert pos[3] is SeatPosition.CUTOFF
    assert pos[4] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=5, dead_small_blind=True)
    assert pos[0] is SeatPosition.BIG_BLIND
    assert pos[1] is SeatPosition.UTG_1
    assert pos[2] is SeatPosition.UTG_2
    assert pos[3] is SeatPosition.CUTOFF
    assert pos[4] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=6)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.UTG_1
    assert pos[3] is SeatPosition.UTG_2
    assert pos[4] is SeatPosition.CUTOFF
    assert pos[5] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=6, dead_small_blind=True)
    assert pos[0] is SeatPosition.BIG_BLIND
    assert pos[1] is SeatPosition.UTG_1
    assert pos[2] is SeatPosition.UTG_2
    assert pos[3] is SeatPosition.UTG_3
    assert pos[4] is SeatPosition.CUTOFF
    assert pos[5] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=7)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.UTG_1
    assert pos[3] is SeatPosition.UTG_2
    assert pos[4] is SeatPosition.UTG_3
    assert pos[5] is SeatPosition.CUTOFF
    assert pos[6] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=7, dead_small_blind=True)
    assert pos[0] is SeatPosition.BIG_BLIND
    assert pos[1] is SeatPosition.UTG_1
    assert pos[2] is SeatPosition.UTG_2
    assert pos[3] is SeatPosition.UTG_3
    assert pos[4] is SeatPosition.MP_1
    assert pos[5] is SeatPosition.CUTOFF
    assert pos[6] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=8)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.UTG_1
    assert pos[3] is SeatPosition.UTG_2
    assert pos[4] is SeatPosition.UTG_3
    assert pos[5] is SeatPosition.MP_1
    assert pos[6] is SeatPosition.CUTOFF
    assert pos[7] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=8, dead_small_blind=True)
    assert pos[0] is SeatPosition.BIG_BLIND
    assert pos[1] is SeatPosition.UTG_1
    assert pos[2] is SeatPosition.UTG_2
    assert pos[3] is SeatPosition.UTG_3
    assert pos[4] is SeatPosition.MP_1
    assert pos[5] is SeatPosition.MP_2
    assert pos[6] is SeatPosition.CUTOFF
    assert pos[7] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=9)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.UTG_1
    assert pos[3] is SeatPosition.UTG_2
    assert pos[4] is SeatPosition.UTG_3
    assert pos[5] is SeatPosition.MP_1
    assert pos[6] is SeatPosition.MP_2
    assert pos[7] is SeatPosition.CUTOFF
    assert pos[8] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=9, dead_small_blind=True)
    assert pos[0] is SeatPosition.BIG_BLIND
    assert pos[1] is SeatPosition.UTG_1
    assert pos[2] is SeatPosition.UTG_2
    assert pos[3] is SeatPosition.UTG_3
    assert pos[4] is SeatPosition.MP_1
    assert pos[5] is SeatPosition.MP_2
    assert pos[6] is SeatPosition.MP_3
    assert pos[7] is SeatPosition.CUTOFF
    assert pos[8] is SeatPosition.BUTTON

    pos = SeatPosition.list_available(player_count=10)
    assert pos[0] is SeatPosition.SMALL_BLIND
    assert pos[1] is SeatPosition.BIG_BLIND
    assert pos[2] is SeatPosition.UTG_1
    assert pos[3] is SeatPosition.UTG_2
    assert pos[4] is SeatPosition.UTG_3
    assert pos[5] is SeatPosition.MP_1
    assert pos[6] is SeatPosition.MP_2
    assert pos[7] is SeatPosition.MP_3
    assert pos[8] is SeatPosition.CUTOFF
    assert pos[9] is SeatPosition.BUTTON

    msg = "Impossible seats configuration: 10 players with dead small blind"
    with pytest.raises(ValueError, match=msg):
        pos = SeatPosition.list_available(player_count=10, dead_small_blind=True)

    msg = "Impossible seats configuration: 11 players"
    with pytest.raises(ValueError, match=msg):
        pos = SeatPosition.list_available(player_count=11)
