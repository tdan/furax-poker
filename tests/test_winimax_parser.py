import datetime

from furax_poker.parser.factory import ParserFactory
from furax_poker.parser import WinamaxParser
from furax_poker.hand import Room


base_parser = ParserFactory.create(Room.WINAMAX)
winamax_parser = WinamaxParser()


def test_parse_hand_header_tournament():
    header = [
        (
            'Winamax Poker - Tournament "Hold\'em [180 Max]" buyIn: 1.81€ +'
            " 0.20€ level: 4 - HandId: #810940995725164559-1-1489863683 - "
            "Holdem no limit (50/100) - 2017/03/18 19:01:23 UTC"
        ),
        (
            "Table: 'Hold'em [180 Max](188811914)#14'"
            " 6-max (real money) Seat #1 is the button"
        ),
    ]
    hand = winamax_parser.parse_hand_header(header)
    assert hand["hand_id"] == "810940995725164559-1-1489863683"
    assert hand["start_datetime"] == datetime.datetime(2017, 3, 18, 19, 1, 23)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "TOURNAMENT"
    assert hand["table_name"] == "Hold'em [180 Max](188811914)#14"
    assert hand["tournament_name"] == "Hold'em [180 Max]"
    assert hand["tournament_id"] == "188811914"
    assert hand["buy_in"] == 2.01
    assert hand["level"] == 4
    assert hand["ante"] == 0
    assert hand["small_blind"] == 50
    assert hand["big_blind"] == 100
    assert hand["max_seats"] == 6
    assert hand["button_seat"] == 1
    assert hand["players"] == []
    assert hand["rounds"] == []
    assert hand["board"] == []


def test_parse_hand_header_cash_game():
    header = [
        (
            "Winamax Poker - CashGame -"
            " HandId: #810940995725164559-1-1489863683"
            " - Holdem no limit (0.01€/0.02€) - 2017/03/18 19:01:23 UTC"
        ),
        (
            "Table: 'SHORT TRACK 0,10€ 07'"
            " 5-max (real money)"
            " Seat #5 is the button"
        ),
    ]
    hand = winamax_parser.parse_hand_header(header)
    assert hand["hand_id"] == "810940995725164559-1-1489863683"
    assert hand["start_datetime"] == datetime.datetime(2017, 3, 18, 19, 1, 23)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "CASH_GAME"
    assert hand["table_name"] == "SHORT TRACK 0,10€ 07"
    assert hand["tournament_name"] is None
    assert hand["tournament_id"] is None
    assert hand["buy_in"] is None
    assert hand["level"] is None
    assert hand["ante"] == 0
    assert hand["small_blind"] == 0.01
    assert hand["big_blind"] == 0.02
    assert hand["max_seats"] == 5
    assert hand["button_seat"] == 5
    assert hand["players"] == []
    assert hand["rounds"] == []
    assert hand["board"] == []


def test_parse_hand_header_go_fast():
    header = [
        (
            'Winamax Poker - Go Fast "Colodaro 3" -'
            " HandId: #810940995725164559-1-1489863683"
            " - Holdem no limit (0.01€/0.02€) - 2017/03/18 19:01:23 UTC"
        ),
        ("Table: 'GO FAST CO-3 2,00€'" " 5-max (real money)" " Seat #5 is the button"),
    ]
    hand = winamax_parser.parse_hand_header(header)
    assert hand["hand_id"] == "810940995725164559-1-1489863683"
    assert hand["start_datetime"] == datetime.datetime(2017, 3, 18, 19, 1, 23)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "CASH_GAME"
    assert hand["table_name"] == "GO FAST CO-3 2,00€"
    assert hand["tournament_name"] is None
    assert hand["tournament_id"] is None
    assert hand["buy_in"] is None
    assert hand["level"] is None
    assert hand["ante"] == 0
    assert hand["small_blind"] == 0.01
    assert hand["big_blind"] == 0.02
    assert hand["max_seats"] == 5
    assert hand["button_seat"] == 5
    assert hand["players"] == []
    assert hand["rounds"] == []
    assert hand["board"] == []


def test_parse_player_line():
    line = "Seat 5: TiennouFurax (10000)"
    player = winamax_parser.parse_player_line(line)
    assert player["name"] == "TiennouFurax"
    assert player["seat"] == 5
    assert player["stack"] == 10000


def test_player_line_with_currency():
    line = "Seat 5: TiennouFurax (0.6€)"
    player = winamax_parser.parse_player_line(line)
    assert player["name"] == "TiennouFurax"
    assert player["seat"] == 5
    assert player["stack"] == 0.6


def test_parse_round_line():
    line = "*** PRE-FLOP ***"
    round = winamax_parser.parse_round_line(line)
    assert round["actions"] == []


def test_parse_board_without_cards():
    line = "Board: []"
    board = winamax_parser.parse_board_line(line)
    assert board == []


def test_parse_board_line_with_cards():
    line = "Board: [7s 8d 6d]"
    board = winamax_parser.parse_board_line(line)
    assert board == ["7s", "8d", "6d"]


def test_parse_hero_line():
    line = "Dealt to TiennouFurax [Jc Jd]"
    hand = winamax_parser.parse_hero_line(line)
    assert hand["hero_name"] == "TiennouFurax"
    assert hand["hero_cards"] == ["Jc", "Jd"]


def test_parse_hero_line_weird_nickname():
    line = "Dealt to Tienn[ou Fu]rax [Jc Jd]"
    hand = winamax_parser.parse_hero_line(line)
    assert hand["hero_name"] == "Tienn[ou Fu]rax"
    assert hand["hero_cards"] == ["Jc", "Jd"]


def test_parse_action_line_ante():
    line = "TiennouFurax posts ante 500"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "ANTE"
    assert action["value"] == 500
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_ante_all_in():
    line = "TiennouFurax posts ante 500 and is all-in"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "ANTE"
    assert action["value"] == 500
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is True


def test_parse_action_line_small_blind():
    line = "TiennouFurax posts small blind 2000"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "SMALL_BLIND"
    assert action["value"] == 2000
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_small_blind_all_in():
    line = "TiennouFurax posts small blind 2000 and is all-in"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "SMALL_BLIND"
    assert action["value"] == 2000
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is True


def test_parse_action_line_big_blind():
    line = "TiennouFurax posts big blind 4000"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "BIG_BLIND"
    assert action["value"] == 4000
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_big_blind_all_in():
    line = "TiennouFurax posts big blind 4000 and is all-in"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "BIG_BLIND"
    assert action["value"] == 4000
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is True


def test_parse_action_line_check():
    line = "TiennouFurax checks"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "CHECK"
    assert action["value"] is None
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_bet():
    line = "TiennouFurax bets 450"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "BET"
    assert action["value"] == 450
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_bet_all_in():
    line = "TiennouFurax bets 450 and is all-in"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "BET"
    assert action["value"] == 450
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is True


def test_parse_action_line_call():
    line = "TiennouFurax calls 450"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "CALL"
    assert action["value"] == 450
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_call_all_in():
    line = "TiennouFurax calls 450 and is all-in"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "CALL"
    assert action["value"] == 450
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is True


def test_parse_action_line_raise():
    line = "TiennouFurax raises 200 to 400"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "RAISE"
    assert action["value"] == 200
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_raise_all_in():
    line = "TiennouFurax raises 200 to 400 and is all-in"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "RAISE"
    assert action["value"] == 200
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is True


def test_parse_action_line_fold():
    line = "TiennouFurax folds"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "FOLD"
    assert action["value"] is None
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_collect():
    line = "TiennouFurax collected 1300 from pot"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "COLLECT"
    assert action["value"] == 1300
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_action_line_show():
    line = "TiennouFurax shows [Ad 2s] (Two pairs : Aces and Queens)"
    action = winamax_parser.parse_action_line(line)
    assert action["type"] == "SHOW"
    assert action["value"] == ["Ad", "2s"]
    assert action["player_name"] == "TiennouFurax"
    assert action["all_in"] is False


def test_parse_without_flop():

    lines = [
        'Winamax Poker - Tournament "Expresso" buyIn: 0.93€ + 0.07€ level: 1 - HandId: '
        "#1516120629399519233-5-1587587112 - Holdem no limit (10/20) - 2020/04/22 "
        "20:25:12 UTC",
        "Table: 'Expresso(352999342)#0' 3-max (real money) Seat #2 is the button",
        "Seat 2: ThunderYann (970)",
        "Seat 3: TiennouFurax (530)",
        "*** ANTE/BLINDS ***",
        "ThunderYann posts small blind 10",
        "TiennouFurax posts big blind 20",
        "Dealt to TiennouFurax [Jc 7s]",
        "*** PRE-FLOP *** ",
        "ThunderYann folds",
        "TiennouFurax collected 30 from pot",
        "*** SUMMARY ***",
        "Total pot 30 | No rake",
        "Seat 3: TiennouFurax (big blind) won 30",
        "",
    ]
    # hands = list(winamax_parser.parse(lines))
    hands = list(base_parser.parse(lines))
    assert len(hands) == 1

    hand = hands[0]
    assert hand["hand_id"] == "1516120629399519233-5-1587587112"
    assert hand["start_datetime"] == datetime.datetime(2020, 4, 22, 20, 25, 12)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "TOURNAMENT"
    assert hand["table_name"] == "Expresso(352999342)#0"
    assert hand["tournament_name"] == "Expresso"
    assert hand["tournament_id"] == "352999342"
    assert hand["buy_in"] == 1.0
    assert hand["level"] == 1
    assert hand["ante"] == 0
    assert hand["small_blind"] == 10
    assert hand["big_blind"] == 20
    assert hand["max_seats"] == 3
    assert hand["button_seat"] == 2
    assert hand["hero_name"] == "TiennouFurax"
    assert hand["hero_cards"] == ["Jc", "7s"]
    assert hand["board"] == []
    assert len(hand["players"]) == 2
    assert hand["players"][0] == {"name": "ThunderYann", "seat": 2, "stack": 970}
    assert hand["players"][1] == {"name": "TiennouFurax", "seat": 3, "stack": 530}
    assert len(hand["rounds"]) == 2
    assert hand["rounds"][0] == {
        "actions": [
            {
                "player_name": "ThunderYann",
                "type": "SMALL_BLIND",
                "value": 10,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "type": "BIG_BLIND",
                "value": 20,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][1] == {
        "actions": [
            {
                "player_name": "ThunderYann",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "type": "COLLECT",
                "value": 30,
                "all_in": False,
            },
        ]
    }


def test_parse_until_showdown():
    lines = [
        'Winamax Poker - Tournament "Hold\'em [180 Max]" buyIn: 1.80€ + 0.20€'
        " level: 6 - HandId: #810940995725164559-51-1489865625 - Holdem no "
        "limit (25/150/300) - 2017/03/18 19:33:45 UTC",
        "Table: 'Hold'em [180 Max](188811914)#14' 6-max (real money) "
        "Seat #3 is the button",
        "Seat 1: Le.Suisse (11700)",
        "Seat 2: JmB0 (13908)",
        "Seat 3: pschitcitron (17197)",
        "Seat 4: RoadToFish (14309)",
        "Seat 5: TiennouFurax (18024)",
        "Seat 6: gautron72 (4162)",
        "*** ANTE/BLINDS ***",
        "RoadToFish posts ante 25",
        "TiennouFurax posts ante 25",
        "Le.Suisse posts ante 25",
        "JmB0 posts ante 25",
        "pschitcitron posts ante 25",
        "gautron72 posts ante 25",
        "RoadToFish posts small blind 150",
        "TiennouFurax posts big blind 300",
        "Dealt to TiennouFurax [Ad 2s]",
        "*** PRE-FLOP *** ",
        "gautron72 folds",
        "Le.Suisse calls 300",
        "JmB0 folds",
        "pschitcitron folds",
        "RoadToFish folds",
        "TiennouFurax checks",
        "*** FLOP *** [Ah 6d 6h]",
        "TiennouFurax checks",
        "Le.Suisse bets 450",
        "TiennouFurax calls 450",
        "*** TURN *** [Ah 6d 6h][Qc]",
        "TiennouFurax checks",
        "Le.Suisse checks",
        "*** RIVER *** [Ah 6d 6h Qc][Qs]",
        "TiennouFurax bets 1200",
        "Le.Suisse calls 1200",
        "*** SHOW DOWN ***",
        "Le.Suisse shows [3c As] (Two pairs : Aces and Queens)",
        "TiennouFurax shows [Ad 2s] (Two pairs : Aces and Queens)",
        "TiennouFurax collected 2100 from pot",
        "Le.Suisse collected 2100 from pot",
        "*** SUMMARY ***",
        "Total pot 4200 | No rake",
        "Board: [Ah 6d 6h Qc Qs]",
        "Seat 1: Le.Suisse showed [3c As] and won 2100 with Two pairs : "
        "Aces and Queens",
        "Seat 5: TiennouFurax (big blind) showed [Ad 2s] and won 2100 with "
        "Two pairs : Aces and Queens",
        "",
    ]
    hands = list(winamax_parser.parse(lines))
    assert len(hands) == 1

    hand = hands[0]
    assert hand["hand_id"] == "810940995725164559-51-1489865625"
    assert hand["start_datetime"] == datetime.datetime(2017, 3, 18, 19, 33, 45)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "TOURNAMENT"
    assert hand["table_name"] == "Hold'em [180 Max](188811914)#14"
    assert hand["tournament_name"] == "Hold'em [180 Max]"
    assert hand["tournament_id"] == "188811914"
    assert hand["buy_in"] == 2.0
    assert hand["level"] == 6
    assert hand["ante"] == 25
    assert hand["small_blind"] == 150
    assert hand["big_blind"] == 300
    assert hand["max_seats"] == 6
    assert hand["button_seat"] == 3
    assert hand["hero_name"] == "TiennouFurax"
    assert hand["hero_cards"] == ["Ad", "2s"]
    assert hand["board"] == ["Ah", "6d", "6h", "Qc", "Qs"]
    assert len(hand["players"]) == 6
    assert hand["players"][0] == {"name": "Le.Suisse", "seat": 1, "stack": 11700}
    assert hand["players"][1] == {"name": "JmB0", "seat": 2, "stack": 13908}
    assert hand["players"][2] == {"name": "pschitcitron", "seat": 3, "stack": 17197}
    assert hand["players"][3] == {"name": "RoadToFish", "seat": 4, "stack": 14309}
    assert hand["players"][4] == {"name": "TiennouFurax", "seat": 5, "stack": 18024}
    assert hand["players"][5] == {"name": "gautron72", "seat": 6, "stack": 4162}
    assert len(hand["rounds"]) == 6
    assert hand["rounds"][0] == {
        "actions": [
            {"player_name": "RoadToFish", "type": "ANTE", "value": 25, "all_in": False},
            {
                "player_name": "TiennouFurax",
                "type": "ANTE",
                "value": 25,
                "all_in": False,
            },
            {"player_name": "Le.Suisse", "type": "ANTE", "value": 25, "all_in": False},
            {"player_name": "JmB0", "type": "ANTE", "value": 25, "all_in": False},
            {
                "player_name": "pschitcitron",
                "type": "ANTE",
                "value": 25,
                "all_in": False,
            },
            {"player_name": "gautron72", "type": "ANTE", "value": 25, "all_in": False},
            {
                "player_name": "RoadToFish",
                "type": "SMALL_BLIND",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "type": "BIG_BLIND",
                "value": 300,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][1] == {
        "actions": [
            {
                "player_name": "gautron72",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {"player_name": "Le.Suisse", "type": "CALL", "value": 300, "all_in": False},
            {"player_name": "JmB0", "type": "FOLD", "value": None, "all_in": False},
            {
                "player_name": "pschitcitron",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "RoadToFish",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][2] == {
        "actions": [
            {
                "player_name": "TiennouFurax",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
            {"player_name": "Le.Suisse", "type": "BET", "value": 450, "all_in": False},
            {
                "player_name": "TiennouFurax",
                "type": "CALL",
                "value": 450,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][3] == {
        "actions": [
            {
                "player_name": "TiennouFurax",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][4] == {
        "actions": [
            {
                "player_name": "TiennouFurax",
                "type": "BET",
                "value": 1200,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "type": "CALL",
                "value": 1200,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][5] == {
        "actions": [
            {
                "player_name": "Le.Suisse",
                "type": "SHOW",
                "value": ["3c", "As"],
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "type": "SHOW",
                "value": ["Ad", "2s"],
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "type": "COLLECT",
                "value": 2100,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "type": "COLLECT",
                "value": 2100,
                "all_in": False,
            },
        ]
    }


def test_parse_multiple_hands():
    lines = [
        'Winamax Poker - Tournament "Hold\'em [180 Max]" buyIn: 1.80€ + 0.20€'
        " level: 6 - HandId: #810940995725164559-51-1489865625 - Holdem no "
        "limit (25/150/300) - 2017/03/18 19:33:45 UTC",
        "Table: 'Hold'em [180 Max](188811914)#14' 6-max (real money) "
        "Seat #3 is the button",
        "Seat 1: Le.Suisse (11700)",
        "Seat 2: JmB0 (13908)",
        "Seat 3: pschitcitron (17197)",
        "Seat 4: RoadToFish (14309)",
        "Seat 5: TiennouFurax (18024)",
        "Seat 6: gautron72 (4162)",
        "*** ANTE/BLINDS ***",
        "RoadToFish posts small blind 150",
        "TiennouFurax posts big blind 300",
        "Dealt to TiennouFurax [Ad 2s]",
        "*** PRE-FLOP *** ",
        "gautron72 folds",
        "Le.Suisse folds",
        "JmB0 folds",
        "pschitcitron folds",
        "RoadToFish folds",
        "TiennouFurax folds",
        "*** SUMMARY ***",
        "Total pot 4200 | No rake",
        "Board: [Ah 6d 6h Qc Qs]",
        "",
        "",
        'Winamax Poker - Tournament "Hold\'em [180 Max]" buyIn: 1.80€ + 0.20€'
        " level: 6 - HandId: #810940995725164559-51-1489865625 - Holdem no "
        "limit (25/150/300) - 2017/03/18 19:33:45 UTC",
        "Table: 'Hold'em [180 Max](188811914)#14' 6-max (real money) "
        "Seat #3 is the button",
        "Seat 1: Le.Suisse (11700)",
        "Seat 2: JmB0 (13908)",
        "Seat 3: pschitcitron (17197)",
        "Seat 4: RoadToFish (14309)",
        "Seat 5: TiennouFurax (18024)",
        "Seat 6: gautron72 (4162)",
        "*** ANTE/BLINDS ***",
        "RoadToFish posts small blind 150",
        "TiennouFurax posts big blind 300",
        "Dealt to TiennouFurax [Ad 2s]",
        "*** PRE-FLOP *** ",
        "gautron72 folds",
        "Le.Suisse folds",
        "JmB0 folds",
        "pschitcitron folds",
        "RoadToFish folds",
        "TiennouFurax folds",
        "*** SUMMARY ***",
        "Total pot 4200 | No rake",
        "Board: [Ah 6d 6h Qc Qs]",
        "",
    ]
    hands = list(winamax_parser.parse(lines))
    assert len(hands) == 2


def test_parse_hand_with_error():
    lines = [
        "BAD HEADER" "limit (25/150/300) - 2017/03/18 19:33:45 UTC",
        "Table: 'Hold'em [180 Max](188811914)#14' 6-max (real money) "
        "Seat #3 is the button",
        "Seat 1: Le.Suisse (11700)",
        "Seat 2: JmB0 (13908)",
        "Seat 3: pschitcitron (17197)",
        "Seat 4: RoadToFish (14309)",
        "Seat 5: TiennouFurax (18024)",
        "Seat 6: gautron72 (4162)",
        "*** ANTE/BLINDS ***",
        "RoadToFish posts small blind 150",
        "TiennouFurax posts big blind 300",
        "Dealt to TiennouFurax [Ad 2s]",
        "*** PRE-FLOP *** ",
        "gautron72 folds",
        "Le.Suisse folds",
        "JmB0 folds",
        "pschitcitron folds",
        "RoadToFish folds",
        "TiennouFurax folds",
        "*** SUMMARY ***",
        "Total pot 4200 | No rake",
        "Board: [Ah 6d 6h Qc Qs]",
        'Winamax Poker - Tournament "Hold\'em [180 Max]" buyIn: 1.80€ + 0.20€'
        " level: 6 - HandId: #810940995725164559-51-1489865625 - Holdem no "
        "limit (25/150/300) - 2017/03/18 19:33:45 UTC",
        "Table: 'Hold'em [180 Max](188811914)#14' 6-max (real money) "
        "Seat #3 is the button",
        "Seat 1: Le.Suisse (11700)",
        "Seat 2: JmB0 (13908)",
        "Seat 3: pschitcitron (17197)",
        "Seat 4: RoadToFish (14309)",
        "Seat 5: TiennouFurax (18024)",
        "Seat 6: gautron72 (4162)",
        "*** ANTE/BLINDS ***",
        "RoadToFish posts small blind 150",
        "TiennouFurax posts big blind 300",
        "Dealt to TiennouFurax [Ad 2s]",
        "*** PRE-FLOP *** ",
        "gautron72 folds",
        "Le.Suisse folds",
        "JmB0 folds",
        "pschitcitron folds",
        "RoadToFish folds",
        "TiennouFurax folds",
        "*** SUMMARY ***",
        "Total pot 4200 | No rake",
        "Board: [Ah 6d 6h Qc Qs]",
        "",
    ]
    hands = list(winamax_parser.parse(lines))
    assert len(hands) == 1
