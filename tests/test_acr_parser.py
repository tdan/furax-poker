import datetime
import pytest

from furax_poker.parser.factory import ParserFactory
from furax_poker.parser import ACRParser
from furax_poker.hand import Room


acr_parser = ACRParser()
base_parser = ParserFactory.create(Room.ACR)


def test_parse_hand_header_cash_game():

    header = [
        ("Hand #1140503139 - Holdem(No Limit) - $0.05/$0.10 - 2022/03/01 03:27:53 UTC"),
        ("Miami Shores 6-max Seat #2 is the button"),
    ]

    hand = acr_parser.parse_hand_header(header)

    assert hand["hand_id"] == "1140503139"
    assert hand["start_datetime"] == datetime.datetime.strptime(
        "2022/03/01 03:27:53 UTC", "%Y/%m/%d %H:%M:%S %Z"
    )
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "CASH_GAME"
    assert hand["table_name"] == "Miami Shores"
    assert hand["small_blind"] == 0.05
    assert hand["big_blind"] == 0.10
    assert hand["max_seats"] == 6
    assert hand["button_seat"] == 2


def test_parse_hand_header_tournament():

    header = [
        (
            "Game Hand #1156401359 - Tournament #26537231 - Holdem(No Limit) - Level 4 (750.00/1500.00)- 2022/03/15 00:26:39 UTC"
        ),
        ("Table '2' 9-max Seat #8 is the button"),
    ]

    hand = acr_parser.parse_hand_header(header)

    assert hand["hand_id"] == "1156401359"
    assert hand["tournament_id"] == "26537231"
    assert hand["start_datetime"] == datetime.datetime.strptime(
        "2022/03/15 00:26:39 UTC", "%Y/%m/%d %H:%M:%S %Z"
    )
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "TOURNAMENT"
    assert hand["table_name"] == "26537231"
    assert hand["level"] == 4
    assert hand["small_blind"] == 750.00
    assert hand["big_blind"] == 1500.00
    assert hand["max_seats"] == 9
    assert hand["button_seat"] == 8


def test_parse_player_line():

    player_line = "Seat 2: raytookmymoney ($10.72)"

    player = acr_parser.parse_player_line(player_line)

    assert player["name"] == "raytookmymoney"
    assert player["seat"] == 2
    assert player["stack"] == 10.72


def test_parse_hero_line():

    hero_line = "Dealt to xelnaga90 [4c Ah]"

    hero = acr_parser.parse_hero_line(hero_line)

    assert hero["hero_name"] == "xelnaga90"
    assert hero["hero_cards"] == ["4c", "Ah"]


# Test correct actions
@pytest.mark.parametrize(
    "action_line, expected_result",
    [
        (
            "KRNegus posts the small blind $0.05",
            {
                "type": "SMALL_BLIND",
                "value": 0.05,
                "player_name": "KRNegus",
                "all_in": False,
            },
        ),
        (
            "xelnaga90 posts the big blind $0.10",
            {
                "type": "BIG_BLIND",
                "value": 0.10,
                "player_name": "xelnaga90",
                "all_in": False,
            },
        ),
        (
            "b41nd34d folds",
            {"type": "FOLD", "value": None, "player_name": "b41nd34d", "all_in": False},
        ),
        (
            "Willtodill checks",
            {
                "type": "CHECK",
                "value": None,
                "player_name": "Willtodill",
                "all_in": False,
            },
        ),
        (
            "KRNegus bets $1.43",
            {"type": "BET", "value": 1.43, "player_name": "KRNegus", "all_in": False},
        ),
        (
            "Willtodill calls $0.72",
            {
                "type": "CALL",
                "value": 0.72,
                "player_name": "Willtodill",
                "all_in": False,
            },
        ),
        (
            "FreakyNeek raises 28401.00 to 28401.00 and is all-in",
            {
                "type": "RAISE",
                "value": 28401,
                "player_name": "FreakyNeek",
                "all_in": True,
            },
        ),
        (
            "xelnaga90 shows [6d 5s] (a pair of Aces [Ah Ac 9h 8d 6d])",
            {
                "type": "SHOW",
                "value": ["6d", "5s"],
                "player_name": "xelnaga90",
                "all_in": False,
            },
        ),
    ],
)
def test_parse_action_line(action_line, expected_result):
    assert acr_parser.parse_action_line(action_line) == expected_result


def test_parse_action_line_exception():
    line = "this line should throw exception"
    with pytest.raises(Exception):
        acr_parser.parse_action_line(line)


def test_parse_board_line():
    line = "Board [Tc 8c 3h 5h]"
    board = acr_parser.parse_board_line(line)

    assert board == ["Tc", "8c", "3h", "5h"]


def test_parse_without_flop():
    lines = [
        "Hand #1159060274 - Holdem(No Limit) - $0.01/$0.02 - 2022/03/17 04:07:52 UTC",
        "Little Creek 9-max Seat #3 is the button",
        "Seat 1: on the rim ($2.00)",
        "Seat 2: GipsyLuck ($2.01)",
        "Seat 3: Mehalb ($0.89)",
        "Seat 4: xelnaga90 ($1.90)",
        "Seat 5: Gareth12 ($1.47)",
        "Seat 6: SaltedRaccoon ($4.85)",
        "Seat 8: RIVERBOATAL ($0.69)",
        "xelnaga90 posts the small blind $0.01",
        "Gareth12 posts the big blind $0.02",
        "on the rim waits for big blind",
        "*** HOLE CARDS ***",
        "Dealt to xelnaga90 [Kd Jc]",
        "SaltedRaccoon folds",
        "RIVERBOATAL folds",
        "GipsyLuck folds",
        "Mehalb folds",
        "xelnaga90 raises $0.06 to $0.07",
        "Gareth12 folds",
        "Uncalled bet ($0.05) returned to xelnaga90",
        "xelnaga90 does not show",
        "*** SUMMARY ***",
        "Total pot $0.04",
        "Seat 2: GipsyLuck folded on the Pre-Flop and did not bet",
        "Seat 3: Mehalb (button) folded on the Pre-Flop",
        "Seat 4: xelnaga90 did not show and won $0.04",
        "Seat 5: Gareth12 (big blind) folded on the Pre-Flop",
        "Seat 6: SaltedRaccoon folded on the Pre-Flop and did not bet",
        "Seat 8: RIVERBOATAL folded on the Pre-Flop and did not bet",
        "",
    ]

    hands = list(base_parser.parse(lines))
    assert len(hands) == 1

    hand = hands[0]
    assert hand["hand_id"] == "1159060274"
    assert hand["start_datetime"] == datetime.datetime(2022, 3, 17, 4, 7, 52)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "CASH_GAME"
    assert hand["table_name"] == "Little Creek"
    assert hand["small_blind"] == 0.01
    assert hand["big_blind"] == 0.02
    assert hand["max_seats"] == 9
    assert hand["button_seat"] == 3
    assert hand["hero_name"] == "xelnaga90"
    assert hand["hero_cards"] == ["Kd", "Jc"]

    assert len(hand["players"]) == 7
    assert hand["players"][0] == {"name": "on the rim", "seat": 1, "stack": 2.00}
    assert hand["players"][1] == {"name": "GipsyLuck", "seat": 2, "stack": 2.01}
    assert hand["players"][3] == {"name": "xelnaga90", "seat": 4, "stack": 1.90}
    assert hand["players"][6] == {"name": "RIVERBOATAL", "seat": 8, "stack": 0.69}

    assert len(hand["rounds"]) == 2
    assert hand["rounds"][0] == {
        "actions": [
            {
                "player_name": "xelnaga90",
                "type": "SMALL_BLIND",
                "value": 0.01,
                "all_in": False,
            },
            {
                "player_name": "Gareth12",
                "type": "BIG_BLIND",
                "value": 0.02,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][1] == {
        "actions": [
            {
                "player_name": "SaltedRaccoon",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "RIVERBOATAL",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "GipsyLuck",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Mehalb",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "xelnaga90",
                "type": "RAISE",
                "value": 0.06,
                "all_in": False,
            },
            {
                "player_name": "Gareth12",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
        ]
    }


def test_parse_until_showdown():
    lines = [
        "Hand #1159068838 - Holdem(No Limit) - $0.01/$0.02 - 2022/03/17 04:16:30 UTC",
        "Little Creek 9-max Seat #8 is the button",
        "Seat 1: on the rim ($2.00)",
        "Seat 2: GipsyLuck ($2.00)",
        "Seat 3: Mehalb ($0.89) is sitting out",
        "Seat 4: xelnaga90 ($2.04)",
        "Seat 5: Gareth12 ($0.90)",
        "Seat 6: SaltedRaccoon ($4.85)",
        "Seat 8: PkrStdnt ($2.00)",
        "Seat 9: yodabob ($2.52)",
        "yodabob posts the small blind $0.01",
        "on the rim posts the big blind $0.02",
        "*** HOLE CARDS ***",
        "Dealt to xelnaga90 [2c 7s]",
        "GipsyLuck folds",
        "xelnaga90 folds",
        "Gareth12 raises $0.06 to $0.06",
        "SaltedRaccoon folds",
        "PkrStdnt folds",
        "yodabob calls $0.05",
        "on the rim calls $0.04",
        "*** FLOP *** [Jh 3h Qc]",
        "Main pot $0.18 | Rake $0.00",
        "yodabob checks",
        "on the rim checks",
        "Gareth12 checks",
        "*** TURN *** [Jh 3h Qc] [Qs]",
        "Main pot $0.18 | Rake $0.00",
        "yodabob checks",
        "on the rim checks",
        "Gareth12 checks",
        "*** RIVER *** [Jh 3h Qc Qs] [7h]",
        "Main pot $0.18 | Rake $0.00",
        "yodabob bets $0.09",
        "on the rim folds",
        "Gareth12 calls $0.09",
        "*** SHOW DOWN ***",
        "Main pot $0.35 | Rake $0.01",
        "Gareth12 shows [9s As] (a pair of Queens [Qs Qc As Jh 9s])",
        "yodabob shows [5s Ks] (a pair of Queens [Qs Qc Ks Jh 7h])",
        "Gareth12 collected $0.35 from main pot",
        "*** SUMMARY ***",
        "Total pot $0.35 | Rake $0.01",
        "Board [Jh 3h Qc Qs 7h]",
        "Seat 1: on the rim (big blind) folded on the River",
        "Seat 2: GipsyLuck folded on the Pre-Flop and did not bet",
        "Seat 4: xelnaga90 folded on the Pre-Flop and did not bet",
        "Seat 5: Gareth12 showed [9s As] and won $0.35 with a pair of Queens [Qs Qc As Jh 9s] with the kicker Ace",
        "Seat 6: SaltedRaccoon folded on the Pre-Flop and did not bet",
        "Seat 8: PkrStdnt (button) folded on the Pre-Flop",
        "Seat 9: yodabob (small blind) showed [5s Ks] and lost with a pair of Queens [Qs Qc Ks Jh 7h]",
        "",
    ]

    hands = list(base_parser.parse(lines))
    assert len(hands) == 1

    hand = hands[0]
    assert hand["hand_id"] == "1159068838"
    assert hand["start_datetime"] == datetime.datetime(2022, 3, 17, 4, 16, 30)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "CASH_GAME"
    assert hand["table_name"] == "Little Creek"
    assert hand["small_blind"] == 0.01
    assert hand["big_blind"] == 0.02
    assert hand["max_seats"] == 9
    assert hand["button_seat"] == 8
    assert hand["hero_name"] == "xelnaga90"
    assert hand["hero_cards"] == ["2c", "7s"]
    assert hand["board"] == ["Jh", "3h", "Qc", "Qs", "7h"]

    assert len(hand["players"]) == 8
    assert hand["players"][0] == {"name": "on the rim", "seat": 1, "stack": 2.00}
    assert hand["players"][1] == {"name": "GipsyLuck", "seat": 2, "stack": 2.00}
    assert hand["players"][3] == {"name": "xelnaga90", "seat": 4, "stack": 2.04}
    assert hand["players"][6] == {"name": "PkrStdnt", "seat": 8, "stack": 2.00}
    assert hand["players"][7] == {"name": "yodabob", "seat": 9, "stack": 2.52}

    assert len(hand["rounds"]) == 6
    assert hand["rounds"][0] == {
        "actions": [
            {
                "player_name": "yodabob",
                "type": "SMALL_BLIND",
                "value": 0.01,
                "all_in": False,
            },
            {
                "player_name": "on the rim",
                "type": "BIG_BLIND",
                "value": 0.02,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][1] == {
        "actions": [
            {
                "player_name": "GipsyLuck",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "xelnaga90",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Gareth12",
                "type": "RAISE",
                "value": 0.06,
                "all_in": False,
            },
            {
                "player_name": "SaltedRaccoon",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "PkrStdnt",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "yodabob",
                "type": "CALL",
                "value": 0.05,
                "all_in": False,
            },
            {
                "player_name": "on the rim",
                "type": "CALL",
                "value": 0.04,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][3] == {
        "actions": [
            {
                "player_name": "yodabob",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "on the rim",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Gareth12",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][5] == {
        "actions": [
            {
                "player_name": "Gareth12",
                "type": "SHOW",
                "value": ["9s", "As"],
                "all_in": False,
            },
            {
                "player_name": "yodabob",
                "type": "SHOW",
                "value": ["5s", "Ks"],
                "all_in": False,
            },
            {
                "player_name": "Gareth12",
                "type": "COLLECT",
                "value": 0.35,
                "all_in": False,
            },
        ]
    }


def test_parse_tournament_until_showdown():
    lines = [
        "Game Hand #1156401359 - Tournament #26537231 - Holdem(No Limit) - Level 4 (750.00/1500.00)- 2022/03/15 00:26:39 UTC",
        "Table '2' 9-max Seat #8 is the button",
        "Seat 1: McCoyUSA1776 (37250.00)",
        "Seat 2: LenKoz (59068.00)",
        "Seat 4: FreakyNeek (31551.00)",
        "Seat 5: xelnaga90 (30000.00)",
        "Seat 6: r23y (61669.00)",
        "Seat 7: Redhog (25170.00)",
        "Seat 8: Jhop55 (24995.00)",
        "Seat 9: marciano20 (27145.00)",
        "McCoyUSA1776 posts ante 150.00",
        "LenKoz posts ante 150.00",
        "FreakyNeek posts ante 150.00",
        "xelnaga90 posts ante 150.00",
        "r23y posts ante 150.00",
        "Redhog posts ante 150.00",
        "Jhop55 posts ante 150.00",
        "marciano20 posts ante 150.00",
        "marciano20 posts the small blind 750.00",
        "McCoyUSA1776 posts the big blind 1500.00",
        "*** HOLE CARDS ***",
        "Main pot 1200.00",
        "Dealt to xelnaga90 [As 3h]",
        "LenKoz folds",
        "FreakyNeek raises 3000.00 to 3000.00",
        "xelnaga90 folds",
        "r23y folds",
        "Redhog calls 3000.00",
        "Jhop55 folds",
        "marciano20 calls 2250.00",
        "McCoyUSA1776 calls 1500.00",
        "*** FLOP *** [5s Jc 4s]",
        "Main pot 13200.00",
        "marciano20 checks",
        "McCoyUSA1776 bets 13200.00",
        "FreakyNeek raises 28401.00 to 28401.00 and is all-in",
        "Redhog folds",
        "marciano20 folds",
        "McCoyUSA1776 calls 15201.00",
        "*** TURN *** [5s Jc 4s] [3c]",
        "Main pot 70002.00",
        "*** RIVER *** [5s Jc 4s 3c] [4h]",
        "Main pot 70002.00",
        "*** SHOW DOWN ***",
        "Main pot 70002.00",
        "McCoyUSA1776 shows [7d 7h] (two pair, Sevens and Fours [7h 7d 4s 4h Jc])",
        "FreakyNeek shows [Qs Jh] (two pair, Jacks and Fours [Jh Jc 4s 4h Qs])",
        "FreakyNeek collected 70002.00 from main pot",
        "*** SUMMARY ***",
        "Total pot 70002.00",
        "Board [5s Jc 4s 3c 4h]",
        "Seat 1: McCoyUSA1776 (big blind) showed [7d 7h] and lost with two pair, Sevens and Fours [7h 7d 4s 4h Jc]",
        "Seat 2: LenKoz folded on the Pre-Flop and did not bet",
        "Seat 4: FreakyNeek showed [Qs Jh] and won 70002.00 with two pair, Jacks and Fours [Jh Jc 4s 4h Qs]",
        "Seat 5: xelnaga90 folded on the Pre-Flop and did not bet",
        "Seat 6: r23y folded on the Pre-Flop and did not bet",
        "Seat 7: Redhog folded on the Flop",
        "Seat 8: Jhop55 (button) folded on the Pre-Flop",
        "Seat 9: marciano20 (small blind) folded on the Flop",
        "",
    ]

    hands = list(base_parser.parse(lines))
    assert len(hands) == 1

    hand = hands[0]
    assert hand["hand_id"] == "1156401359"
    assert hand["tournament_id"] == "26537231"
    assert hand["tournament_name"] == "26537231"
    assert hand["start_datetime"] == datetime.datetime(2022, 3, 15, 0, 26, 39)
    assert hand["variant"] == "NO_LIMIT_HOLDEM"
    assert hand["game_type"] == "TOURNAMENT"
    assert hand["table_name"] == "26537231"
    assert hand["level"] == 4
    assert hand["small_blind"] == 750.00
    assert hand["big_blind"] == 1500.00
    assert hand["max_seats"] == 9
    assert hand["button_seat"] == 8
    assert hand["hero_name"] == "xelnaga90"
    assert hand["hero_cards"] == ["As", "3h"]
    assert hand["board"] == ["5s", "Jc", "4s", "3c", "4h"]

    assert len(hand["players"]) == 8
    assert hand["players"][0] == {"name": "McCoyUSA1776", "seat": 1, "stack": 37250.00}
    assert hand["players"][1] == {"name": "LenKoz", "seat": 2, "stack": 59068.00}
    assert hand["players"][3] == {"name": "xelnaga90", "seat": 5, "stack": 30000.00}
    assert hand["players"][6] == {"name": "Jhop55", "seat": 8, "stack": 24995.00}
    assert hand["players"][7] == {"name": "marciano20", "seat": 9, "stack": 27145.00}

    assert len(hand["rounds"]) == 6
    assert hand["rounds"][0] == {
        "actions": [
            {
                "player_name": "McCoyUSA1776",
                "type": "ANTE",
                "value": 150.00,
                "all_in": False,
            },
            {
                "player_name": "LenKoz",
                "type": "ANTE",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "FreakyNeek",
                "type": "ANTE",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "xelnaga90",
                "type": "ANTE",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "r23y",
                "type": "ANTE",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "Redhog",
                "type": "ANTE",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "Jhop55",
                "type": "ANTE",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "marciano20",
                "type": "ANTE",
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "marciano20",
                "type": "SMALL_BLIND",
                "value": 750.00,
                "all_in": False,
            },
            {
                "player_name": "McCoyUSA1776",
                "type": "BIG_BLIND",
                "value": 1500.00,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][1] == {
        "actions": [
            {
                "player_name": "LenKoz",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "FreakyNeek",
                "type": "RAISE",
                "value": 3000.00,
                "all_in": False,
            },
            {
                "player_name": "xelnaga90",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "r23y",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Redhog",
                "type": "CALL",
                "value": 3000.00,
                "all_in": False,
            },
            {
                "player_name": "Jhop55",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "marciano20",
                "type": "CALL",
                "value": 2250.00,
                "all_in": False,
            },
            {
                "player_name": "McCoyUSA1776",
                "type": "CALL",
                "value": 1500.00,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][2] == {
        "actions": [
            {
                "player_name": "marciano20",
                "type": "CHECK",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "McCoyUSA1776",
                "type": "BET",
                "value": 13200.00,
                "all_in": False,
            },
            {
                "player_name": "FreakyNeek",
                "type": "RAISE",
                "value": 28401.00,
                "all_in": True,
            },
            {
                "player_name": "Redhog",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "marciano20",
                "type": "FOLD",
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "McCoyUSA1776",
                "type": "CALL",
                "value": 15201.00,
                "all_in": False,
            },
        ]
    }
    assert hand["rounds"][3] == {"actions": []}
    assert hand["rounds"][5] == {
        "actions": [
            {
                "player_name": "McCoyUSA1776",
                "type": "SHOW",
                "value": ["7d", "7h"],
                "all_in": False,
            },
            {
                "player_name": "FreakyNeek",
                "type": "SHOW",
                "value": ["Qs", "Jh"],
                "all_in": False,
            },
            {
                "player_name": "FreakyNeek",
                "type": "COLLECT",
                "value": 70002.00,
                "all_in": False,
            },
        ]
    }


def test_parse_multiple_hands():
    lines = [
        "Hand #1159071344 - Holdem(No Limit) - $0.01/$0.02 - 2022/03/17 04:19:07 UTC",
        "Little Creek 9-max Seat #9 is the button",
        "Seat 1: on the rim ($1.94) is sitting out",
        "Seat 2: GipsyLuck ($2.00)",
        "Seat 4: xelnaga90 ($2.04)",
        "Seat 5: Gareth12 ($1.10)",
        "Seat 6: SaltedRaccoon ($4.85)",
        "Seat 8: PkrStdnt ($2.00)",
        "Seat 9: yodabob ($2.37)",
        "GipsyLuck posts the big blind $0.02",
        "*** HOLE CARDS ***",
        "Dealt to xelnaga90 [Kh 8c]",
        "xelnaga90 calls $0.02",
        "Gareth12 folds",
        "SaltedRaccoon folds",
        "PkrStdnt folds",
        "yodabob calls $0.02",
        "GipsyLuck checks",
        "*** FLOP *** [9s Qs 7h]",
        "Main pot $0.06 | Rake $0.00",
        "GipsyLuck checks",
        "xelnaga90 checks",
        "yodabob checks",
        "*** TURN *** [9s Qs 7h] [4s]",
        "Main pot $0.06 | Rake $0.00",
        "GipsyLuck checks",
        "xelnaga90 checks",
        "yodabob bets $0.02",
        "GipsyLuck calls $0.02",
        "xelnaga90 folds",
        "*** RIVER *** [9s Qs 7h 4s] [7c]",
        "Main pot $0.10 | Rake $0.00",
        "GipsyLuck bets $0.12",
        "yodabob calls $0.12",
        "*** SHOW DOWN ***",
        "Main pot $0.33 | Rake $0.01",
        "GipsyLuck shows [7s 8h] (three of a kind, Set of Sevens [7s 7h 7c Qs 9s])",
        "yodabob shows [Qd 9d] (two pair, Queens and Nines [Qs Qd 9s 9d 7h])",
        "GipsyLuck collected $0.33 from main pot",
        "*** SUMMARY ***",
        "Total pot $0.33 | Rake $0.01",
        "Board [9s Qs 7h 4s 7c]",
        "Seat 2: GipsyLuck (big blind) showed [7s 8h] and won $0.33 with three of a kind, Set of Sevens [7s 7h 7c Qs 9s]",
        "Seat 4: xelnaga90 folded on the Turn",
        "Seat 5: Gareth12 folded on the Pre-Flop and did not bet",
        "Seat 6: SaltedRaccoon folded on the Pre-Flop and did not bet",
        "Seat 8: PkrStdnt folded on the Pre-Flop and did not bet",
        "Seat 9: yodabob (button) showed [Qd 9d] and lost with two pair, Queens and Nines [Qs Qd 9s 9d 7h]",
        "",
        "Hand #1159072657 - Holdem(No Limit) - $0.01/$0.02 - 2022/03/17 04:20:30 UTC",
        "Little Creek 9-max Seat #1 is the button",
        "Seat 1: on the rim ($1.94) is sitting out",
        "Seat 2: GipsyLuck ($2.17)",
        "Seat 4: xelnaga90 ($2.02)",
        "Seat 5: Gareth12 ($1.10)",
        "Seat 6: SaltedRaccoon ($4.85)",
        "Seat 7: MistahFab ($2.00)",
        "Seat 8: PkrStdnt ($2.00)",
        "Seat 9: yodabob ($2.21)",
        "GipsyLuck posts the small blind $0.01",
        "xelnaga90 posts the big blind $0.02",
        "MistahFab waits for big blind",
        "*** HOLE CARDS ***",
        "Dealt to xelnaga90 [Qd Kd]",
        "Gareth12 folds",
        "SaltedRaccoon folds",
        "PkrStdnt raises $0.06 to $0.06",
        "yodabob folds",
        "GipsyLuck folds",
        "xelnaga90 raises $0.18 to $0.20",
        "PkrStdnt folds",
        "Uncalled bet ($0.14) returned to xelnaga90",
        "xelnaga90 does not show",
        "*** SUMMARY ***",
        "Total pot $0.13",
        "Seat 2: GipsyLuck (small blind) folded on the Pre-Flop",
        "Seat 4: xelnaga90 did not show and won $0.13",
        "Seat 5: Gareth12 folded on the Pre-Flop and did not bet",
        "Seat 6: SaltedRaccoon folded on the Pre-Flop and did not bet",
        "Seat 8: PkrStdnt folded on the Pre-Flop and did not bet",
        "Seat 9: yodabob folded on the Pre-Flop and did not bet",
        "",
    ]
    hands = list(base_parser.parse(lines))
    assert len(hands) == 2


def test_parse_hand_with_error():
    lines = [
        "BAD HEADER Holdem(No Limit) - $0.01/$0.02 - 2022/03/17 04:20:30 UTC",
        "Little Creek 9-max Seat #1 is the button",
        "Seat 1: on the rim ($1.94) is sitting out",
        "Seat 2: GipsyLuck ($2.17)",
        "Seat 4: xelnaga90 ($2.02)",
        "Seat 5: Gareth12 ($1.10)",
        "Seat 6: SaltedRaccoon ($4.85)",
        "Seat 7: MistahFab ($2.00)",
        "Seat 8: PkrStdnt ($2.00)",
        "Seat 9: yodabob ($2.21)",
        "GipsyLuck posts the small blind $0.01",
        "xelnaga90 posts the big blind $0.02",
        "MistahFab waits for big blind",
        "*** HOLE CARDS ***",
        "Dealt to xelnaga90 [Qd Kd]",
        "Gareth12 folds",
        "SaltedRaccoon folds",
        "PkrStdnt raises $0.06 to $0.06",
        "yodabob folds",
        "GipsyLuck folds",
        "xelnaga90 raises $0.18 to $0.20",
        "PkrStdnt folds",
        "Uncalled bet ($0.14) returned to xelnaga90",
        "xelnaga90 does not show",
        "*** SUMMARY ***",
        "Total pot $0.13",
        "Seat 2: GipsyLuck (small blind) folded on the Pre-Flop",
        "Seat 4: xelnaga90 did not show and won $0.13",
        "Seat 5: Gareth12 folded on the Pre-Flop and did not bet",
        "Seat 6: SaltedRaccoon folded on the Pre-Flop and did not bet",
        "Seat 8: PkrStdnt folded on the Pre-Flop and did not bet",
        "Seat 9: yodabob folded on the Pre-Flop and did not bet",
        "",
        "Hand #1159060274 - Holdem(No Limit) - $0.01/$0.02 - 2022/03/17 04:07:52 UTC",
        "Little Creek 9-max Seat #3 is the button",
        "Seat 1: on the rim ($2.00)",
        "Seat 2: GipsyLuck ($2.01)",
        "Seat 3: Mehalb ($0.89)",
        "Seat 4: xelnaga90 ($1.90)",
        "Seat 5: Gareth12 ($1.47)",
        "Seat 6: SaltedRaccoon ($4.85)",
        "Seat 8: RIVERBOATAL ($0.69)",
        "xelnaga90 posts the small blind $0.01",
        "Gareth12 posts the big blind $0.02",
        "on the rim waits for big blind",
        "*** HOLE CARDS ***",
        "Dealt to xelnaga90 [Kd Jc]",
        "SaltedRaccoon folds",
        "RIVERBOATAL folds",
        "GipsyLuck folds",
        "Mehalb folds",
        "xelnaga90 raises $0.06 to $0.07",
        "Gareth12 folds",
        "Uncalled bet ($0.05) returned to xelnaga90",
        "xelnaga90 does not show",
        "*** SUMMARY ***",
        "Total pot $0.04",
        "Seat 2: GipsyLuck folded on the Pre-Flop and did not bet",
        "Seat 3: Mehalb (button) folded on the Pre-Flop",
        "Seat 4: xelnaga90 did not show and won $0.04",
        "Seat 5: Gareth12 (big blind) folded on the Pre-Flop",
        "Seat 6: SaltedRaccoon folded on the Pre-Flop and did not bet",
        "Seat 8: RIVERBOATAL folded on the Pre-Flop and did not bet",
        "",
    ]
    hands = list(base_parser.parse(lines))
    assert len(hands) == 1
