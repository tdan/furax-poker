from furax_poker.analyzer import analyze_action, analyze_cbet, analyze_vpip
from furax_poker.fact import FactType
from furax_poker.hand import ActionType, RoundType, SeatPosition


def test_analyze_action(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        flop=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.CALL),
        ],
        turn=[("Player 1", ActionType.CHECK), ("Hero", ActionType.CHECK)],
        river=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.RAISE),
            ("Hero", ActionType.FOLD),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [FactType.HAND_PLAYED], 1: [FactType.HAND_PLAYED]},
        RoundType.PREFLOP: {
            0: [FactType.PREFLOP_FOLD, FactType.HAND_PLAYED],
            1: [FactType.PREFLOP_FOLD, FactType.HAND_PLAYED],
            2: [FactType.PREFLOP_RAISE, FactType.HAND_PLAYED],
            3: [FactType.PREFLOP_CALL],
            4: [FactType.PREFLOP_FOLD],
        },
        RoundType.FLOP: {
            0: [FactType.FLOP_CHECK],
            1: [FactType.FLOP_BET],
            2: [FactType.FLOP_CALL],
        },
        RoundType.TURN: {0: [FactType.TURN_CHECK], 1: [FactType.TURN_CHECK]},
        RoundType.RIVER: {
            0: [FactType.RIVER_CHECK],
            1: [FactType.RIVER_BET],
            2: [FactType.RIVER_RAISE],
            3: [FactType.RIVER_FOLD],
        },
    }

    analyze_action(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_multiple_actions_preflop(hand_builder):
    """Test that hand played fact is only emitted once even if there are multiple
    pre-flop actions for a player.
    """
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.CALL),
            ("Player 1", ActionType.RAISE),
            ("Player 2", ActionType.FOLD),
            ("Hero", ActionType.CALL),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [FactType.HAND_PLAYED], 1: [FactType.HAND_PLAYED]},
        RoundType.PREFLOP: {
            0: [FactType.PREFLOP_FOLD, FactType.HAND_PLAYED],
            1: [FactType.PREFLOP_FOLD, FactType.HAND_PLAYED],
            2: [FactType.PREFLOP_CALL, FactType.HAND_PLAYED],
            3: [FactType.PREFLOP_RAISE],
            4: [FactType.PREFLOP_FOLD],
            5: [FactType.PREFLOP_CALL],
        },
    }

    analyze_action(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_only_collect_preflop(hand_builder):
    """Test that hand played fact is created even if player just collect the pot during
    pre-flop.
    """
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.FOLD),
            ("Player 1", ActionType.FOLD),
            ("Player 2", ActionType.COLLECT),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [FactType.HAND_PLAYED], 1: [FactType.HAND_PLAYED]},
        RoundType.PREFLOP: {
            0: [FactType.PREFLOP_FOLD, FactType.HAND_PLAYED],
            1: [FactType.PREFLOP_FOLD, FactType.HAND_PLAYED],
            2: [FactType.PREFLOP_FOLD, FactType.HAND_PLAYED],
            3: [FactType.PREFLOP_FOLD],
            4: [],
        },
    }

    analyze_action(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_vpip_classic(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        # Define actions on flop to test no facts are generated for them
        flop=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.CALL),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {
            0: [FactType.VPIP_CHANCE],
            1: [FactType.VPIP_CHANCE],
            2: [FactType.VPIP, FactType.PFR],
            3: [FactType.VPIP],
            4: [FactType.VPIP_CHANCE],
        },
        RoundType.FLOP: {0: [], 1: [], 2: []},
    }

    analyze_vpip(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_vpip_multiple_raise_same_round(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.RAISE),
            ("Player 2", ActionType.FOLD),
            ("Hero", ActionType.CALL),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {
            0: [FactType.VPIP_CHANCE],
            1: [FactType.VPIP_CHANCE],
            2: [FactType.VPIP, FactType.PFR],
            3: [FactType.VPIP, FactType.PFR],
            4: [FactType.VPIP_CHANCE],
            5: [],
        },
    }

    analyze_vpip(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_vpip_everybody_folds(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.FOLD),
            ("Player 1", ActionType.FOLD),
            ("Player 2", ActionType.COLLECT),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {
            0: [FactType.VPIP_CHANCE],
            1: [FactType.VPIP_CHANCE],
            2: [FactType.VPIP_CHANCE],
            3: [FactType.VPIP_CHANCE],
            4: [],
        },
    }

    analyze_vpip(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_cbet_called(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        flop=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.CALL),
        ],
        turn=[("Player 1", ActionType.CHECK), ("Hero", ActionType.CHECK)],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {0: [], 1: [], 2: [], 3: [], 4: []},
        RoundType.FLOP: {0: [], 1: [FactType.CBET], 2: [FactType.FOLD_TO_CBET_CHANCE]},
        RoundType.TURN: {0: [], 1: []},
    }

    analyze_cbet(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_cbet_family_pot(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.CALL),
        ],
        flop=[
            ("Player 1", ActionType.CHECK),
            ("Player 2", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        turn=[("Player 1", ActionType.CHECK), ("Hero", ActionType.CHECK)],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {0: [], 1: [], 2: [], 3: [], 4: []},
        RoundType.FLOP: {
            0: [],
            1: [],
            2: [FactType.CBET],
            3: [FactType.FOLD_TO_CBET_CHANCE],
            4: [FactType.FOLD_TO_CBET],
        },
        RoundType.TURN: {0: [], 1: []},
    }

    analyze_cbet(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_cbet_raised(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        flop=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.RAISE),
            ("Hero", ActionType.CALL),
        ],
        turn=[("Player 1", ActionType.CHECK), ("Hero", ActionType.CHECK)],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {0: [], 1: [], 2: [], 3: [], 4: []},
        RoundType.FLOP: {
            0: [],
            1: [FactType.CBET],
            2: [FactType.FOLD_TO_CBET_CHANCE],
            3: [],
        },
        RoundType.TURN: {0: [], 1: []},
    }

    analyze_cbet(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_cbet_folded(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        flop=[
            ("Player 1", ActionType.CHECK),
            ("Hero", ActionType.BET),
            ("Player 1", ActionType.FOLD),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {0: [], 1: [], 2: [], 3: [], 4: []},
        RoundType.FLOP: {0: [], 1: [FactType.CBET], 2: [FactType.FOLD_TO_CBET]},
    }

    analyze_cbet(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_cbet_donk_bet(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.RAISE),
            ("Player 1", ActionType.CALL),
            ("Player 2", ActionType.FOLD),
        ],
        flop=[("Player 1", ActionType.BET), ("Hero", ActionType.CALL)],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {0: [], 1: [], 2: [], 3: [], 4: []},
        RoundType.FLOP: {0: [], 1: []},
    }

    analyze_cbet(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_cbet_no_preflop_raise(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.CHECK),
            ("Player 1", ActionType.FOLD),
            ("Player 2", ActionType.CHECK),
        ],
        flop=[("Player 1", ActionType.BET), ("Hero", ActionType.CALL)],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {0: [], 1: [], 2: [], 3: [], 4: []},
        RoundType.FLOP: {0: [], 1: []},
    }

    analyze_cbet(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)


def test_analyze_cbet_no_flop(hand_builder):
    hand = hand_builder.create(
        seating_plan=[
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (4, "Player 4"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
        preflop=[
            ("Player 3", ActionType.FOLD),
            ("Player 4", ActionType.FOLD),
            ("Hero", ActionType.FOLD),
            ("Player 1", ActionType.FOLD),
            ("Player 2", ActionType.COLLECT),
        ],
    )

    expected_fact_types = {
        RoundType.BLINDS: {0: [], 1: []},
        RoundType.PREFLOP: {0: [], 1: [], 2: [], 3: [], 4: []},
    }

    analyze_cbet(hand)
    for round in hand.rounds:
        for action in round.actions:
            expected_types = expected_fact_types[round.type][action.order]
            assert all([fact.action is action for fact in action.facts])
            assert set([fact.type for fact in action.facts]) == set(expected_types)
