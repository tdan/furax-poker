import datetime
import logging
from pathlib import Path
from threading import Thread
import time

from PyQt5.QtCore import pyqtSignal, QObject
from watchdog.events import FileDeletedEvent, FileSystemEvent, FileSystemEventHandler
from watchdog.observers import Observer

from furax_poker.analyzer import analyze_hand
from furax_poker.db import session_scope
from furax_poker.hand import Hand, Room
from furax_poker.loader import load_hand
from furax_poker.parser.factory import ParserFactory
from furax_poker.settings import History, HistoryFile

logger = logging.getLogger(__name__)


class Tracker(QObject):
    """Orchestrates synchronization and monitoring of history files with database.

    It spawns two children processes per history. The first one, called the
    synchronizer, will perform a "batch" synchronization ; the second one, called the
    observer, is a long running process whose role is to monitor history files changes.
    Tracker orchestrates these two and allow to listen to particular signals such as
    synchronization progress or notification when a new hand was loaded in database.

    Synchronization or monitoring strategies can be customized by passing classes for
    synchronizer and observer objects to tracker constructor.
    """

    sync_started = pyqtSignal()
    load_progress = pyqtSignal(float)
    analyze_progress = pyqtSignal(float)
    sync_finished = pyqtSignal()
    refreshed = pyqtSignal()
    hand_loaded = pyqtSignal(object)
    table_joined = pyqtSignal(object)

    def __init__(self, synchronizer_class=None, observer_class=None):
        super().__init__()
        self.synchronizer_class = synchronizer_class or SynchronizerThread
        self.observer_class = observer_class or ObserverThread
        self._children_registry = {
            self.synchronizer_class: {},
            self.observer_class: {},
        }
        self.tables = set()

    def start(self):
        """Starts synchronization and monitoring of all history entries in database."""
        logger.info("Tracker started")
        with session_scope() as session:
            for history in session.query(History):
                self._start_sync(history)
                self._start_observe(history)

    def stop(self):
        """Stops all synchronization and monitoring tasks."""
        for class_, registry in self._children_registry.items():
            for history in list(registry.keys()):
                self._stop_child(class_, history)
        logger.info("Tracker stopped")

    def restart(self):
        """Restarts history synchronization and monitoring, using new database state."""
        self.stop()
        self.start()

    def _start_sync(self, history: History):
        """Starts synchroning given history folder."""
        synchronizer = self._spawn_child(self.synchronizer_class, history)
        for signal in [
            "sync_started",
            "load_progress",
            "analyze_progress",
            "sync_finished",
        ]:
            self._forward_signal(synchronizer, signal)

        synchronizer.start()

    def _stop_sync(self, history: History):
        """Stops synchroning given history folder."""
        self._stop_child(self.synchronizer_class, history)

    def _start_observe(self, history: History):
        """Starts monitoring given history folder."""
        observer = self._spawn_child(self.observer_class, history)
        for signal in ["refreshed", "hand_loaded"]:
            self._forward_signal(observer, signal)
        observer.hand_loaded.connect(self._on_hand_loaded)

        observer.start()

    def _stop_observe(self, history: History):
        """Stops monitoring given history folder."""
        self._stop_child(self.observer_class, history)

    def _spawn_child(self, class_, history: History):
        """Spawns a child of given class and store it in internal registry."""
        registry = self._children_registry[class_]
        if history not in registry.keys():
            child = class_(history)
            registry[history] = child
            return child

    def _stop_child(self, class_, history: History):
        """Stops a child of given class and remove it from internal registry."""
        registry = self._children_registry[class_]
        child = registry.pop(history, None)
        if child and child.is_alive():
            child.stop()
            return child

    def _forward_signal(self, src: QObject, signal: str):
        """Forward signal from child into application main loop."""
        getattr(src, signal).connect(getattr(self, signal).emit)

    def _on_hand_loaded(self, hand: Hand):
        table = hand.table
        new_table = table.id not in self.tables
        not_tournament = table.tournament_id is None
        new_tournament = table.tournament_id not in [
            t.tournament_id for t in self.tables
        ]
        if new_table:
            # Emit signal only if it's not a new table for an existing tournament,
            # otherwise just replace current tournament table by the new one.
            if not_tournament or new_tournament:
                self.table_joined.emit(table)
            else:
                self.tables = {
                    t for t in self.tables if t.tournament_id != table.tournament_id
                }
            self.tables.add(table)


class SynchronizerThread(Thread, QObject):
    """Thread synchronizing a history folder with database.

    Basically, it compares history files on user system with parsed files stored in
    database. For all new files and files which have changed since last
    synchronization, parses them, loads and analyzes new hands.
    """

    sync_started = pyqtSignal()
    load_progress = pyqtSignal(float)
    analyze_progress = pyqtSignal(float)
    sync_finished = pyqtSignal()

    def __init__(self, history: History):
        super().__init__()
        super(Thread, self).__init__()
        self.should_stop = False
        self.history = history
        self.parser = ParserFactory.create(history.room)

    def run(self):
        """Synchronize history files with database.

        This process is designed to allow monitoring its progress. Thus, the two main
        steps (loading and analyzing) are decoupled and both know the number of items
        they have to process.
        """
        start = time.time()
        logger.info(f"Starting synchronization of {self.history}")
        self.sync_started.emit()

        with session_scope(write=True) as session:
            # Reattach history to session (needed to update HistoryFile attributes)
            self.history = session.merge(self.history)

            # First, determine which files have changed since last synchronization
            history_files_to_parse = []
            history_files_by_path = {f.path: f for f in self.history.files}
            for path in Path(self.history.path).glob("*"):
                if is_history_file(self.history.room, path):
                    history_file = history_files_by_path.get(str(path), None)
                    if history_file is None:
                        history_file = HistoryFile(
                            path=str(path), history_id=self.history.id
                        )
                        session.add(history_file)

                    last_modification_datetime = get_last_modification_datetime(path)
                    if (
                        history_file.last_sync_datetime is None
                        or history_file.last_sync_datetime < last_modification_datetime
                    ):
                        history_files_to_parse.append(history_file)

            if history_files_to_parse:
                logger.info(
                    f"Will parse {len(history_files_to_parse)} files for {self.history}"
                )
                progress = 0
                self.load_progress.emit(0)
            else:
                logger.info(f"No new file to parse for {self.history}")

        # Then, parse all files and load new hands
        for history_file in history_files_to_parse:
            if self.should_stop:
                return
            try:
                path = Path(history_file.path)
                with path.open(encoding="utf-8") as f:
                    parsing_succeed = True
                    for raw_hand in self.parser.parse(f):
                        # Use one transaction per hand to skip hands that couldn't be
                        # loaded for any reason, and to let other thread take the
                        # writing lock.
                        try:
                            with session_scope(write=True) as session:
                                hand = load_hand(session, self.history.room, raw_hand)
                                if hand:
                                    logger.info(f"Loaded {hand}")
                        except Exception:
                            logger.exception(
                                f"An error occured while loading hand "
                                f"from {history_file}"
                            )
                            parsing_succeed = False
            except Exception:
                logger.exception(f"An error occured while parsing {history_file}")
                parsing_succeed = False

            # Do not update last sync datetime if any error occured in order to
            # retry on next synchronization process
            if parsing_succeed:
                with session_scope(write=True) as session:
                    (
                        session.query(HistoryFile)
                        .filter_by(id=history_file.id)
                        .update(
                            {"last_sync_datetime": get_last_modification_datetime(path)}
                        )
                    )
            progress += 1
            self.load_progress.emit(progress / len(history_files_to_parse))

        with session_scope() as session:
            # Get ids of hand that are not analyzed
            # Note that we don't load the whole Hand object here to prevent an exessive
            # usage of memory on first synchronization of big history.
            hand_ids_to_analyze = session.query(Hand.id).filter_by(analyzed=False).all()
            if hand_ids_to_analyze:
                logger.info(
                    f"Will analyze {len(hand_ids_to_analyze)} hands for {self.history}"
                )
                progress = 0
                self.analyze_progress.emit(0)
            else:
                logger.info(f"No hand to analyze for {self.history}")

        for (hand_id,) in hand_ids_to_analyze:
            if self.should_stop:
                return
            try:
                with session_scope(write=True) as session:
                    hand = session.query(Hand).filter_by(id=hand_id).one()
                    analyze_hand(session, hand)
                    hand.analyzed = True
                    logger.info(f"Analyzed {hand}")
            except Exception:
                logger.exception(f"An error occured while analyzing {hand}")

            progress += 1
            self.analyze_progress.emit(progress / len(hand_ids_to_analyze))

        duration = round(time.time() - start, 2)
        logger.info(f"Completed synchronization of {self.history} in {duration}s")
        self.sync_finished.emit()

    def stop(self):
        """Stops thread in the middle of synchronization process."""
        if self.is_alive():
            logger.info("Stopping synchronization of {self.history}")
            self.should_stop = True
            self.join()


class ObserverThread(Observer, FileSystemEventHandler, QObject):
    """Tread monitoring file changes in a history directory.

    When a file is created or modified in history, the thread will check if it is
    relevant to parse, then it will load and analyze new hands.
    """

    refreshed = pyqtSignal()
    hand_loaded = pyqtSignal(object)

    def __init__(self, history: History):
        super().__init__()
        super(FileSystemEventHandler, self).__init__()
        self.history = history
        self.parser = ParserFactory.create(history.room)

        logger.info(f"Start monitoring {self.history}")
        self.schedule(self, self.history.path)

    def on_any_event(self, event: FileSystemEvent):
        """Parses file if needed then loads and analyzes new hands."""
        path = Path(event.src_path)
        if (
            path.is_dir()
            or isinstance(event, FileDeletedEvent)
            or not is_history_file(self.history.room, path)
        ):
            return

        logger.info(f'Observer received event "{event.event_type}" for {path}')

        # Open a transaction scope but commit session at each step to prevent a
        # single "unsupported" hand from blocking the analysis of the whole file.
        with session_scope(write=True) as session:
            history_file = (
                session.query(HistoryFile)
                .filter_by(path=str(path), history_id=self.history.id)
                .first()
            )
            if not history_file:
                history_file = HistoryFile(path=str(path), history_id=self.history.id)
                session.add(history_file)

            parsing_succeed = True
            try:
                with path.open(encoding="utf-8") as f:
                    for raw_hand in self.parser.parse(f):
                        hand = None
                        try:
                            session.begin_nested()
                            hand = load_hand(session, self.history.room, raw_hand)
                            session.commit()
                            if hand:
                                logger.info(f"Loaded {hand}")
                        except Exception:
                            session.rollback()
                            logger.exception(
                                f"An error occured while loading "
                                f"hand from {history_file}"
                            )
                            parsing_succeed = False

                        # If hand was loaded, analyze it
                        if hand is not None:
                            try:
                                session.begin_nested()
                                analyze_hand(session, hand)
                                hand.analyzed = True
                                session.commit()
                                logger.info(f"Analyzed {hand}")
                                self.hand_loaded.emit(hand)
                            except Exception:
                                session.rollback()
                                logger.exception(
                                    f"An error occured while analyzing {hand}"
                                )
            except Exception:
                logger.exception(f"An error occured while parsing {history_file}")
                parsing_succeed = False

            # Do not update last sync datetime if any error occured in order to
            # retry on next synchronization process
            if parsing_succeed:
                history_file.last_sync_datetime = get_last_modification_datetime(path)

        self.refreshed.emit()


def is_history_file(room: Room, path: Path):
    """Returns a boolean indicating if file at given path is a history file."""
    if room is Room.WINAMAX:
        return "summary" not in path.name and path.suffix == ".txt"
    if room is Room.ACR:
        return "HH" in path.name and path.suffix == ".txt"
    return False


def get_last_modification_datetime(path: Path):
    """Returns last modification datetime of file at given path."""
    return datetime.datetime.fromtimestamp(path.stat().st_mtime)
