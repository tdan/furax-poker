import datetime
import re
from typing import List, Optional, TypedDict, Union

import logging

logger = logging.getLogger(__name__)


class RawPlayer(TypedDict):
    name: str
    seat: int
    stack: float


class RawAction(TypedDict):
    type: str
    value: Optional[Union[float, List[str]]]
    player_name: str
    all_in: bool


class RawRound(TypedDict):
    actions: List[RawAction]


class RawHand(TypedDict):
    hand_id: str
    start_datetime: datetime.datetime
    game_type: str
    variant: str
    tournament_name: Optional[str]
    tournament_id: Optional[str]
    table_name: str
    buy_in: Optional[float]
    level: Optional[int]
    ante: float
    small_blind: float
    big_blind: float
    max_seats: int
    button_seat: int
    players: List[RawPlayer]
    rounds: List[RawRound]
    board: List[str]
    hero_name: Optional[str]
    hero_cards: Optional[List[str]]


def parse_cards(s: str) -> List[str]:
    return [c for c in s.split(" ") if len(c)]


def clean_price(s: str) -> str:
    """Remove currency from string."""
    match = re.match(r"\D*([\d\.]+)", s)
    if match is None:
        raise Exception("bad price %s", s)
    return match.group(1)
