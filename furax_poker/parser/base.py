from abc import ABC, abstractmethod
from typing import Iterable


class Parser(ABC):
    @abstractmethod
    def parse(self, txt: Iterable[str]):
        raise NotImplementedError()
