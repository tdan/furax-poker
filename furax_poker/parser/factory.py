from .base import Parser
from furax_poker.hand import Room

from .winimax_parser import WinamaxParser
from .acr_parser import ACRParser


class ParserFactory:
    @staticmethod
    def create(room: Room) -> Parser:
        registry = {Room.WINAMAX: WinamaxParser(), Room.ACR: ACRParser()}
        return registry[room]
