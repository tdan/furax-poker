import enum

from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Integer,
    String,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship

from furax_poker.db import Base
from furax_poker.hand import Room


@enum.unique
class TimeScope(enum.Enum):
    ALL_TIME = enum.auto()
    CURRENT_SESSION = enum.auto()


class ApplicationSettings(Base):
    """Contains application global settings."""

    autostart_hud = Column(Boolean, nullable=False, default=True)
    favorite_seat = Column(Boolean, nullable=False, default=True)
    hide_hud_when_window_not_active = Column(Boolean, nullable=False, default=True)
    follow_active_window = Column(Boolean, nullable=False, default=True)
    latest_acknowledged_version = Column(String, nullable=True)
    hero_time_scope: TimeScope = Column(
        Enum(TimeScope), nullable=False, default=TimeScope.ALL_TIME
    )  # type: ignore
    opponent_time_scope: TimeScope = Column(
        Enum(TimeScope), nullable=False, default=TimeScope.ALL_TIME
    )  # type: ignore

    def __repr__(self):
        return (
            f"{self.__class__.__name__}(id={self.id}, "
            f"autostart_hud={self.autostart_hud}, "
            f"favorite_seat={self.favorite_seat}, "
            f"hide_hud_when_window_not_active={self.hide_hud_when_window_not_active}, "
            f"follow_active_window={self.follow_active_window}, "
            f"latest_acknowledged_version={self.latest_acknowledged_version},"
            f"hero_time_scope={self.hero_time_scope},"
            f"opponent_time_scope={self.opponent_time_scope})"
        )


class History(Base):
    """Represents a directory containing hand history files."""

    __table_args__ = (UniqueConstraint("path", "room"),)

    path = Column(String, nullable=False, index=True)
    room: Room = Column(Enum(Room), nullable=False, index=True)  # type: ignore
    hero = Column(String, nullable=True)
    files = relationship(
        "HistoryFile", back_populates="history", lazy="joined", cascade="all,delete"
    )

    def __repr__(self):
        return f"{self.__class__.__name__}(id={self.id}, path={self.path})"


class HistoryFile(Base):
    """Represents a hand history file."""

    __table_args__ = (UniqueConstraint("history_id", "path"),)

    path = Column(String, nullable=False, index=True)
    last_sync_datetime = Column(DateTime(), nullable=True, index=True)
    history_id = Column(Integer, ForeignKey("history.id"), nullable=False, index=True)
    history = relationship("History", back_populates="files")

    def __repr__(self):
        return f"{self.__class__.__name__}(id={self.id}, path={self.path})"
