from configparser import ConfigParser

from .utils import resource_path

__version__ = "1.0.0"

config = ConfigParser()
config.read(resource_path("furax_poker.ini"))
