from dataclasses import dataclass
import logging
import requests
from threading import Thread

from PyQt5.QtCore import pyqtSignal, QObject, QUrl
from PyQt5.QtGui import QDesktopServices

from furax_poker import __version__, config
from furax_poker.db import session_scope
from furax_poker.settings import ApplicationSettings

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class ApplicationRelease:
    """Class representing an application release."""

    version: str
    description: str


class UpdateManager(Thread, QObject):
    """Thread checking if a new release if available."""

    new_release_available = pyqtSignal(object)

    api_url = config["APP"]["REPOSITORY_API_URL"]
    download_url = config["APP"]["DOWNLOAD_URL"]

    def __init__(self, settings: ApplicationSettings):
        super().__init__()
        super(Thread, self).__init__()
        self.settings = settings

    def check(self):
        self.start()

    def run(self):
        if self.api_url is not None and self.download_url is not None:
            try:
                url = f"{self.api_url}/releases"
                response = requests.get(url, timeout=5).json()[-1]
                latest = ApplicationRelease(
                    version=response["tag_name"],
                    description=response["description_html"],
                )
                if (
                    latest.version != __version__
                    and latest.version != self.settings.latest_acknowledged_version
                ):
                    self.new_release_available.emit(latest)
            except Exception:
                logger.exception("Couldn't check if a new release is available")

    def download(self, release: ApplicationRelease):
        if self.download_url:
            QDesktopServices.openUrl(QUrl(self.download_url))

    def acknowledge(self, release: ApplicationRelease):
        with session_scope(write=True) as session:
            self.settings = session.merge(self.settings)
            self.settings.latest_acknowledged_version = release.version
