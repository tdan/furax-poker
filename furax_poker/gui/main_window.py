from typing import TYPE_CHECKING

from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QAction, QMainWindow, QStackedLayout, QWidget

from furax_poker.gui.empty_view import EmptyView
from furax_poker.gui.sync_indicator import SyncIndicator
from furax_poker.gui.table_list_view import TableListView
from furax_poker.gui.utils import UiAttrs
from furax_poker.hand import Table
from furax_poker.utils import resource_path

if TYPE_CHECKING:
    from furax_poker.gui.application import Application


class MainWindow(QMainWindow, UiAttrs):
    def __init__(self, app: "Application"):
        super().__init__()
        self.app = app

        self.loadUi(resource_path("data/ui/main_window.ui"))

        fileMenu = self.menuBar().addMenu("&File")

        settingsAction = QAction("&Settings", self)
        settingsAction.triggered.connect(self.app.openSettingsDialog)
        settingsAction.setShortcut(QKeySequence("Ctrl+Alt+s"))
        fileMenu.addAction(settingsAction)

        helpAction = QAction("&Help", self)
        helpAction.triggered.connect(self.app.openHelp)
        helpAction.setShortcut(QKeySequence("F1"))
        fileMenu.addAction(helpAction)

        aboutAction = QAction("About", self)
        aboutAction.triggered.connect(self.app.openAboutDialog)
        fileMenu.addAction(aboutAction)

        quitAction = QAction("&Quit", self)
        quitAction.triggered.connect(self.app.quit)
        fileMenu.addAction(quitAction)
        quitAction.setShortcut(QKeySequence("Ctrl+q"))

        self.statusBar().addPermanentWidget(SyncIndicator(self.app))

        self._layout = QStackedLayout()
        self._layout.addWidget(EmptyView(self.app))
        self._layout.addWidget(TableListView(self.app))
        self._layout.setCurrentIndex(0)
        widget = QWidget()
        widget.setLayout(self._layout)
        self.setCentralWidget(widget)

        self.app.tracker.table_joined.connect(self._onTableJoined)

    def close(self):
        self.app.quit()

    def _onTableJoined(self, table: Table):
        self._layout.setCurrentIndex(1)
        self.app.tracker.table_joined.disconnect(self._onTableJoined)
