from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeyEvent, QMouseEvent
from PyQt5.QtWidgets import QMenu, QWidget


class ContextMenu(QMenu):
    """Context menu kept open when user selects an action.

    Event handlers are overriden to prevent menu from closing when user has triggered an
    action. Inspired by https://stackoverflow.com/a/12393400/5222444.
    """

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.ignoreNextHide = False

    def setVisible(self, show: bool):
        if not show and self.ignoreNextHide:
            self.ignoreNextHide = False
        else:
            super().setVisible(show)

    def mouseReleaseEvent(self, event: QMouseEvent):
        self.ignoreNextHide = True
        super().mouseReleaseEvent(event)

    def keyPressEvent(self, event: QKeyEvent):
        if event.key() == Qt.Key_Enter:
            self.ignoreNextHide = True
        super().keyPressEvent(event)
