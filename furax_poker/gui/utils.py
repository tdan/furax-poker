import signal
from typing import get_type_hints, Optional

from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtCore import QObject, QTimer
from PyQt5.uic import loadUi


class UiAttrs(QObject):
    """Mixin simplifying loading ui file into a custom widget class.

    It provides the loadUi method that load a ui file and assign widget children by
    inspecting class attribute type hints. For instance, if a widget class has a class
    attribute named "myLabel" with a type hint QLabel, it will look for a "myLabel"
    object with type QLabel in the ui file using findChild.
    """

    def loadUi(self: QObject, filename: str):
        loadUi(filename, self)
        for name, hint in get_type_hints(self.__class__).items():
            if issubclass(hint, QWidget):
                setattr(self, name, self.findChild(hint, name))


def format_int(value: Optional[float], if_none="-"):
    if value is None:
        return if_none
    return "{:.0f}".format(value)


def format_float(value: Optional[float], if_none="-"):
    if value is None:
        return if_none
    return "{:.1f}".format(value)


def format_percent(value: Optional[float], obs=None, if_none="-"):
    if value is None:
        return if_none
    formatted = "{:.0%}".format(value)
    if obs is None:
        return formatted
    return f"{formatted} ({format_int(obs)})"


def format_big_number(value: Optional[float], if_none="-"):
    if value is None:
        return if_none
    suffix = ""
    for _suffix in ["K", "M"]:
        if value >= 1000:
            value /= 1000
            suffix = _suffix
    return f"{format_int(value)}{suffix}"


def setup_interrupt_handling(app: QApplication):
    """Setup handling of KeyboardInterrupt (Ctrl-C) for PyQt.

    See: https://coldfix.eu/2016/11/08/pyqt-boilerplate/
    """

    def quit_app(signum, fram):
        app.quit()

    signal.signal(signal.SIGINT, quit_app)
    signal.signal(signal.SIGTERM, quit_app)

    # Regularly run some (any) python code, so the signal handler gets a
    # chance to be executed:
    _safe_timer(50, lambda: None)


def _safe_timer(timeout, func, *args, **kwargs):
    """Create a timer that is safe against garbage collection and overlapping calls.

    See: http://ralsina.me/weblog/posts/BB974.html
    """

    def timer_event():
        try:
            func(*args, **kwargs)
        finally:
            QTimer.singleShot(timeout, timer_event)

    QTimer.singleShot(timeout, timer_event)
