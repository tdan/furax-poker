import logging
from pathlib import Path
from typing import TYPE_CHECKING

from PyQt5.QtWidgets import (
    QCheckBox,
    QComboBox,
    QDialog,
    QDialogButtonBox,
    QFileDialog,
    QLineEdit,
    QRadioButton,
    QToolButton,
    QWidget,
)
from PyQt5.QtCore import Qt

from furax_poker.db import session_scope
from furax_poker.gui.utils import UiAttrs
from furax_poker.hand import Room
from furax_poker.settings import ApplicationSettings, History, TimeScope
from furax_poker.utils import resource_path

if TYPE_CHECKING:
    from furax_poker.gui.application import Application

logger = logging.getLogger(__name__)


class SettingsDialog(QDialog, UiAttrs):
    _browseButton: QToolButton
    _historyFolderLineEdit: QLineEdit
    _roomComboBox: QComboBox
    _heroNicknameLineEdit: QLineEdit
    _autostartHudCheckBox: QCheckBox
    _favoriteSeatCheckBox: QCheckBox
    _hideHudWhenWindowNotActiveCheckBox: QCheckBox
    _followActiveWindowCheckBox: QCheckBox
    _heroAllTimeRadioButton: QRadioButton
    _heroCurrentSessionRadioButton: QRadioButton
    _opponentHudAllTimeRadioButton: QRadioButton
    _opponentHudCurrentSessionRadioButton: QRadioButton
    _buttonBox: QDialogButtonBox

    def __init__(self, parent: QWidget, app: "Application"):
        logger.info("Open settings dialog")
        super().__init__(parent)
        self.app = app

        self.loadUi(resource_path("data/ui/settings_dialog.ui"))

        self._browseButton.clicked.connect(self._browse)
        self._buttonBox.button(QDialogButtonBox.Reset).clicked.connect(self.reset)
        self._buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self.accept)
        self._buttonBox.button(QDialogButtonBox.Discard).clicked.connect(self.reject)

        self._roomComboBox.insertItem(0, "Winamax", Room.WINAMAX.name)
        self._roomComboBox.insertItem(1, "ACR", Room.ACR.name)
        self._roomComboBox.setCurrentIndex(0)  # Auto select first room

        self.reset()

    def accept(self):
        """Saves changes and closes dialog."""
        logger.info("Apply settings dialog changes")
        with session_scope(write=True) as session:
            history = session.query(History).first()
            if not history:
                history = History()
                session.add(history)

            history.room = Room[self._roomComboBox.currentData()]
            history.hero = self._heroNicknameLineEdit.text() or None
            history.path = self._historyFolderLineEdit.text()

            settings = session.query(ApplicationSettings).first()
            settings.autostart_hud = (
                self._autostartHudCheckBox.checkState() == Qt.Checked
            )
            settings.favorite_seat = (
                self._favoriteSeatCheckBox.checkState() == Qt.Checked
            )
            settings.hide_hud_when_window_not_active = (
                self._hideHudWhenWindowNotActiveCheckBox.checkState() == Qt.Checked
            )
            settings.follow_active_window = (
                self._followActiveWindowCheckBox.checkState() == Qt.Checked
            )
            settings.hero_time_scope = (
                TimeScope.ALL_TIME
                if self._heroAllTimeRadioButton.isChecked()
                else TimeScope.CURRENT_SESSION
            )
            settings.opponent_time_scope = (
                TimeScope.ALL_TIME
                if self._opponentHudAllTimeRadioButton.isChecked()
                else TimeScope.CURRENT_SESSION
            )

        self.app.tracker.restart()
        super().accept()

    def reject(self):
        """Discards changes and closes dialog."""
        logger.info("Cancel settings dialog changes")
        super().reject()

    def reset(self):
        # Fill form with values stored in database
        with session_scope() as session:
            history = session.query(History).first()
            if history:
                self._historyFolderLineEdit.setText(str(Path(history.path)))
                self._roomComboBox.setCurrentIndex(
                    self._roomComboBox.findData(history.room.name)
                )
                self._heroNicknameLineEdit.setText(history.hero or "")

            settings = session.query(ApplicationSettings).one()
            self._autostartHudCheckBox.setCheckState(
                Qt.Checked if settings.autostart_hud else Qt.Unchecked
            )
            self._favoriteSeatCheckBox.setCheckState(
                Qt.Checked if settings.favorite_seat else Qt.Unchecked
            )
            self._hideHudWhenWindowNotActiveCheckBox.setCheckState(
                Qt.Checked if settings.hide_hud_when_window_not_active else Qt.Unchecked
            )
            self._followActiveWindowCheckBox.setCheckState(
                Qt.Checked if settings.follow_active_window else Qt.Unchecked
            )

            if settings.hero_time_scope == TimeScope.ALL_TIME:
                self._heroAllTimeRadioButton.setChecked(True)
            else:
                self._heroCurrentSessionRadioButton.setChecked(True)

            if settings.opponent_time_scope == TimeScope.ALL_TIME:
                self._opponentHudAllTimeRadioButton.setChecked(True)
            else:
                self._opponentHudCurrentSessionRadioButton.setChecked(True)

    def _browse(self):
        response = QFileDialog.getExistingDirectory(
            self, "Select hand history folder", self._historyFolderLineEdit.text()
        )
        if response:
            self._historyFolderLineEdit.setText(response)
