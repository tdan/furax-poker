import argparse
import logging
import logging.config
import shutil
import sys
import threading

from furax_poker.db import init_db, init_app_data_dir, session_scope
from furax_poker.gui.application import Application
from furax_poker.gui.utils import setup_interrupt_handling
from furax_poker.hand import Room
from furax_poker.settings import ApplicationSettings, History
from furax_poker.utils import resource_path

logging.config.fileConfig(resource_path("logging.conf"), disable_existing_loggers=False)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument(
    "-v",
    "--verbose",
    help="Set log level to DEBUG and log SQL queries",
    action="store_true",
)
parser.add_argument(
    "--no-data",
    dest="persistent",
    help=(
        "Do not use existing application data, use a temporary folder so data won't be "
        "persistent. Useful for debugging purpose."
    ),
    action="store_false",
)
parser.add_argument(
    "--history",
    help=(
        "If provided, will use given path as history folder. This option has effect "
        "only when used along with --no-data."
    ),
)


def main():
    args = parser.parse_args()
    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    logger.info("Starting...")
    app_data_dir = init_app_data_dir("furax-poker", temporary=not args.persistent)
    init_db(app_data_dir, "data.db", debug=args.verbose)

    # Create initial settings if provided and running without persistence
    if not args.persistent and args.history:
        with session_scope(write=True) as session:
            session.add(ApplicationSettings(autostart_hud=True))
            session.add(History(path=args.history, room=Room.WINAMAX))

    # Create application
    application = Application()

    # Handle KeyboardInterrupt
    setup_interrupt_handling(application)

    # Run main loop
    application.exec_()

    # Clean up
    if not args.persistent:
        shutil.rmtree(app_data_dir)


def handle_uncaught_exception(exc_type, exc_value, exc_traceback):
    """Handler for unhandled exceptions that will write to the logs"""
    if issubclass(exc_type, KeyboardInterrupt):
        # call the default excepthook saved at __excepthook__
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logger.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


def handle_uncaught_exception_from_thread(args):
    handle_uncaught_exception(args.exc_type, args.exc_value, args.exc_traceback)


sys.excepthook = handle_uncaught_exception
threading.excepthook = handle_uncaught_exception_from_thread


if __name__ == "__main__":
    main()
