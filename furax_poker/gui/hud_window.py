import logging
from typing import cast, Optional

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QMouseEvent
from PyQt5.QtWidgets import (
    QAction,
    QActionGroup,
    QApplication,
    QLabel,
    QStackedLayout,
    QWidget,
)

from furax_poker.fact import FactMetrics
from furax_poker.gui.context_menu import ContextMenu
from furax_poker.gui.utils import (
    format_float,
    format_percent,
    format_big_number,
    UiAttrs,
)
from furax_poker.hand import Seat
from furax_poker.hud import Hud
from furax_poker.settings import TimeScope
from furax_poker.utils import resource_path

logger = logging.getLogger(__name__)


class HudWindow(QWidget):
    """Window displaying metrics for a player on a poker table.

    HUD window has highly customized design and behaviour in order to be always
    displayed on top of other windows, while remaining discrete to avoid disturbing
    user. Thus, this window has no title bar and can be dragged on the screen by
    clicking on it. It also has a system to display multiple views and switching between
    them by clicking on the window.
    """

    def __init__(self, hud: Hud):
        super().__init__()
        self.setWindowFlags(
            cast(
                Qt.WindowFlags,
                Qt.Tool
                | Qt.FramelessWindowHint
                | Qt.NoDropShadowWindowHint
                | Qt.WindowStaysOnTopHint
                | Qt.WindowDoesNotAcceptFocus,
            )
        )
        self.setAttribute(Qt.WA_AlwaysShowToolTips)
        self.setAttribute(Qt.WA_ShowWithoutActivating)

        self.setProperty("class", "fp-hud-window")

        self.seat: Optional[Seat] = None
        self.hud = hud

        # View management
        self._mainView = HudWindowMainView()
        self._secondaryView = HudWindowSecondaryView()
        self._layout = QStackedLayout()
        self._layout.addWidget(self._mainView)
        self._layout.addWidget(self._secondaryView)
        self._layout.setCurrentIndex(0)
        self.setLayout(self._layout)

        # Click and drag custom behaviour
        self._lastX = 0
        self._lastY = 0
        self._clicked = False
        self._moving = False

    @property
    def x(self):
        return super().x()

    @property
    def y(self):
        return super().y()

    @property
    def is_visible(self):
        return self.isVisible()

    def refresh(self, seat: Seat, metrics: FactMetrics):
        self.seat = seat
        self._mainView.refresh(seat, metrics)
        self._secondaryView.refresh(seat, metrics)

        # Makes window as small as possible
        self.resize(1, 1)

    def move(self, x: int, y: int):  # type: ignore
        # Override default move by keeping window on screen, it is done automatically
        # by X11 but on Windows, if a window goes offscreen, it is not updated
        # anymore until a mouse event occurs. It's like window manager detects that
        # the window is offscreen and that it should not need to be updated when
        # there is no user interaction. It's just a guess here, but keeping the
        # window on the screen is the best fix that could be found, and makes app
        # behave consistently on Linux and Windows.
        screen = QApplication.desktop().availableGeometry()
        x = min(max(x, screen.x()), screen.x() + screen.width() - self.width())
        y = min(max(y, screen.y()), screen.y() + screen.height() - self.height())
        super().move(x, y)

    def mousePressEvent(self, event: QMouseEvent):
        self._clicked = True
        self._lastX = event.x()
        self._lastY = event.y()

    def mouseReleaseEvent(self, event: QMouseEvent):
        self._moving = False
        if self._clicked:
            self._layout.setCurrentIndex((self._layout.currentIndex() + 1) % 2)
        self._clicked = False

    def mouseMoveEvent(self, event: QMouseEvent):
        if self._clicked or self._moving:
            self._clicked = False
            self._moving = True
            self.move(event.globalX() - self._lastX, event.globalY() - self._lastY)

    def contextMenuEvent(self, event):
        """Opens a context menu enabling user to change HUD local settings."""
        menu = ContextMenu(self)

        menu.addSeparator().setText("Hero HUD")
        heroAllTime = QAction("All time statistics", self)
        heroAllTime.triggered.connect(
            lambda: setattr(self.hud, "hero_time_scope", TimeScope.ALL_TIME)
        )
        heroAllTime.setCheckable(True)
        heroCurrentSession = QAction("Current session statistics", self)
        heroCurrentSession.triggered.connect(
            lambda: setattr(self.hud, "hero_time_scope", TimeScope.CURRENT_SESSION)
        )
        heroCurrentSession.setCheckable(True)
        heroTimeScope = QActionGroup(self)
        heroTimeScope.addAction(heroAllTime)
        heroTimeScope.addAction(heroCurrentSession)
        menu.addAction(heroAllTime)
        menu.addAction(heroCurrentSession)
        if self.hud.hero_time_scope == TimeScope.ALL_TIME:
            heroAllTime.setChecked(True)
        else:
            heroCurrentSession.setChecked(True)

        menu.addSeparator().setText("Opponent HUD")
        opponentAllTime = QAction("All time statistics", self)
        opponentAllTime.setCheckable(True)
        opponentAllTime.triggered.connect(
            lambda: setattr(self.hud, "opponent_time_scope", TimeScope.ALL_TIME)
        )
        opponentAllTime.setChecked(True)
        opponentCurrentSession = QAction("Current session statistics", self)
        opponentCurrentSession.triggered.connect(
            lambda: setattr(self.hud, "opponent_time_scope", TimeScope.CURRENT_SESSION)
        )
        opponentCurrentSession.setCheckable(True)
        opponentTimeScope = QActionGroup(self)
        opponentTimeScope.addAction(opponentAllTime)
        opponentTimeScope.addAction(opponentCurrentSession)
        menu.addAction(opponentAllTime)
        menu.addAction(opponentCurrentSession)
        if self.hud.opponent_time_scope == TimeScope.ALL_TIME:
            opponentAllTime.setChecked(True)
        else:
            opponentCurrentSession.setChecked(True)

        menu.exec_(self.mapToGlobal(event.pos()))


class HudWindowMainView(QWidget, UiAttrs):
    _playerLabel: QLabel
    _handPlayedLabel: QLabel
    _vpipLabel: QLabel
    _pfrLabel: QLabel
    _afLabel: QLabel

    def __init__(self):
        super().__init__()
        self.loadUi(resource_path("data/ui/hud_window_main_view.ui"))

    def refresh(self, seat: Seat, metrics: FactMetrics):
        self._playerLabel.setText(seat.player.name)
        self._handPlayedLabel.setText(format_big_number(metrics.hand_played))
        self._vpipLabel.setText(format_percent(metrics.vpip_rate))
        self._pfrLabel.setText(format_percent(metrics.pfr_rate))
        self._afLabel.setText(format_float(metrics.af))


class HudWindowSecondaryView(QWidget, UiAttrs):
    _playerLabel: QLabel
    _handPlayedLabel: QLabel
    _cbetLabel: QLabel
    _foldToCbetLabel: QLabel

    def __init__(self):
        super().__init__()
        self.loadUi(resource_path("data/ui/hud_window_secondary_view.ui"))

    def refresh(self, seat: Seat, metrics: FactMetrics):
        self._playerLabel.setText(seat.player.name)
        self._handPlayedLabel.setText(format_big_number(metrics.hand_played))
        self._cbetLabel.setText(format_percent(metrics.cbet_rate, obs=metrics.cbet_obs))
        self._foldToCbetLabel.setText(
            format_percent(metrics.fold_to_cbet_rate, obs=metrics.fold_to_cbet_obs)
        )
