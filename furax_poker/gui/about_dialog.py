import logging

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QDialog, QLabel, QWidget

from furax_poker import __version__, config
from furax_poker.gui.utils import UiAttrs
from furax_poker.utils import resource_path

logger = logging.getLogger(__name__)


class AboutDialog(QDialog, UiAttrs):
    websiteUrl = config["APP"]["WEBSITE_URL"]

    _versionLabel: QLabel
    _websiteLabel: QLabel

    def __init__(self, parent: QWidget):
        logger.info("Open about dialog")
        super().__init__(parent)

        self.loadUi(resource_path("data/ui/about_dialog.ui"))
        self._versionLabel.setText(f"Current version: {__version__}")
        self._websiteLabel.setText(self.websiteUrl or "")
        self._websiteLabel.linkActivated.connect(self._openWebsite)

    def _openWebsite(self):
        QDesktopServices.openUrl(QUrl(self.websiteUrl))
