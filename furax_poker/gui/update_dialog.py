import logging

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QLabel, QWidget

from furax_poker import __version__
from furax_poker.gui.utils import UiAttrs
from furax_poker.update_manager import ApplicationRelease
from furax_poker.utils import resource_path

logger = logging.getLogger(__name__)


class UpdateDialog(QDialog, UiAttrs):
    _descriptionLabel: QLabel
    _currentVersionLabel: QLabel
    _buttonBox: QDialogButtonBox

    def __init__(self, parent: QWidget, release: ApplicationRelease):
        logger.info("Open update dialog")
        super().__init__(parent)

        self.loadUi(resource_path("data/ui/update_dialog.ui"))
        self._currentVersionLabel.setText(f"Current version: {__version__}")
        self._descriptionLabel.setText(release.description)

        downloadButton = self._buttonBox.addButton(
            "Download", QDialogButtonBox.AcceptRole
        )
        ignoreButton = self._buttonBox.addButton("Ignore", QDialogButtonBox.RejectRole)

        downloadButton.clicked.connect(self.accept)
        ignoreButton.clicked.connect(self.reject)
