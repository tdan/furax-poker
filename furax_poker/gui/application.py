import logging
from typing import Dict

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QApplication, QStyleFactory

from furax_poker import config
from furax_poker.active_window import ActiveWindowObserver
from furax_poker.db import session_scope
from furax_poker.gui.about_dialog import AboutDialog
from furax_poker.gui.hud_window import HudWindow
from furax_poker.gui.main_window import MainWindow
from furax_poker.gui.update_dialog import UpdateDialog
from furax_poker.gui.settings_dialog import SettingsDialog
from furax_poker.hud import Hud
from furax_poker.hand import Table
from furax_poker.settings import ApplicationSettings
from furax_poker.tracker import Tracker
from furax_poker.update_manager import ApplicationRelease, UpdateManager
from furax_poker.utils import resource_path

logger = logging.getLogger(__name__)


class Application(QApplication):
    def __init__(self):
        logger.info("Initializing Application...")
        super().__init__([])
        self.aboutToQuit.connect(self._cleanUp)

        self.tracker = Tracker()
        self.activeWindowObserver = ActiveWindowObserver()
        self.settings = self._reloadSettings()
        self.updateManager = UpdateManager(self.settings)
        self._hudByTableId: Dict[int, Hud] = {}

        self.setStyle(QStyleFactory.create("Fusion"))
        with open(resource_path("data/styles.css")) as f:
            self.setStyleSheet(f.read())

        self.updateManager.new_release_available.connect(self._showUpdateDialog)

        self._mainWindow = MainWindow(self)
        self._mainWindow.show()

        self.tracker.start()
        self.activeWindowObserver.start()
        self.updateManager.check()
        logger.info("Application started")

    def openSettingsDialog(self):
        accepted = SettingsDialog(self._mainWindow, self).exec_()
        if accepted:
            self.settings = self._reloadSettings()
            for hud in self._hudByTableId.values():
                hud.settings = self.settings

    def openAboutDialog(self):
        AboutDialog(self._mainWindow).exec_()

    def openHelp(self):
        QDesktopServices.openUrl(QUrl(config["APP"]["WEBSITE_URL"]))

    def startHud(self, table: Table):
        if table.id not in self._hudByTableId:
            hud = Hud(
                self.tracker,
                self.activeWindowObserver,
                self.settings,
                table,
                window_factory=HudWindow,
            )
            hud.start()
            self._hudByTableId[table.id] = hud

    def stopHud(self, table: Table):
        hud = self._hudByTableId.pop(table.id)
        hud.stop()

    def _showUpdateDialog(self, release: ApplicationRelease):
        accepted = UpdateDialog(self._mainWindow, release).exec_()
        if accepted:
            self.updateManager.download(release)
        self.updateManager.acknowledge(release)

    def _cleanUp(self):
        self.tracker.stop()
        self.activeWindowObserver.stop()
        logger.info("Application stopped")

    def _reloadSettings(self) -> ApplicationSettings:
        with session_scope() as session:
            settings = session.query(ApplicationSettings).first()
        if not settings:
            with session_scope(write=True) as session:
                settings = ApplicationSettings()
                session.add(settings)
        return settings
